#import "TH1D.h"

using namespace std;

TH1D* translateTH1D(TH1D* &h, double trans) { // modulo bin width, roughly
	h->Sumw2(kFALSE);
	int n_bins = h->GetXaxis()->GetNbins();
	double bin_width = h->GetXaxis()->GetBinWidth(1); // all bins must have same width
	double size_h = n_bins*bin_width;
	int n_trans = [int] (trans/bin_width);
	double mod_trans = bin_width*n_trans);

	TH1D* hc = (TH1D*)h->Clone();
	hc->Reset();
	for (int i=1; i <= n_bins; i++) {
		double bin_center = h->GetBinCenter(i);
		double bin_content = h->GetBinContent(i);
		f_gauss->SetParameters(bin_center, sigma);
		hc->Fill(bin_center + mod_trans, bin_content);
		double bin_error = h->GetBinError(i)
		hc->SetBinError(i+n_trans, bin_error);
	}
	return hc;
}
