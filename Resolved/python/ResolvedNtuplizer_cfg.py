import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("ResolvedNtuplizer")

process.load("FWCore.MessageService.MessageLogger_cfi")

options = VarParsing.VarParsing('analysis')
options.outputFile = 'resolved_ntuple.root'

#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/user/grauco/B2GAnaFW/B2GAnaFW_80X_V2p1/TT_TuneCUETP8M1_13TeV-powheg-pythia8/B2GAnaFW_80X_V2p1/161021_085128/0000/B2GEDMNtuple_1.root'
#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/user/grauco/B2GAnaFW/B2GAnaFW_80X_V2p1/TT_TuneCUETP8M1_13TeV-powheg-pythia8/B2GAnaFW_80X_V2p1/161021_085128/0000/B2GEDMNtuple_106.root'
options.inputFiles = 'file:/cms/threejet/duncan/crab_dump/Sg500To3J/miniaod_sg500to3j/170904_155330/0000/miniaod_1.root'
options.maxEvents = -1

options.register('reportEvery',
                 1000000, # default value
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.int,
                 "Number of events to process before reporting progress.")
options.register('isSignal',
				True,
				VarParsing.VarParsing.multiplicity.singleton,
				VarParsing.VarParsing.varType.bool,
				"Is the sample resonant.")

options.parseArguments()

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
)
process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(
    options.reportEvery)

process.source = cms.Source(
    "PoolSource",
    fileNames = cms.untracked.vstring(options.inputFiles)
)
process.options = cms.untracked.PSet(
	SkipEvent = cms.untracked.vstring('ProductNotFound')
)

process.load('ThreeJetAnalysis.Resolved.resolvedntuplizer_cfi')

process.resolvedntuplizer.output_file_name = cms.string(options.outputFile)
process.resolvedntuplizer.is_signal = cms.bool(options.isSignal)
process.p = cms.Path(process.resolvedntuplizer)
