// -*- C++ -*-
//
// Package:    ThreeJetAnalysis/Resolved
// Class:      ResolvedNtuplizer
//
/**\class ResolvedNtuplizer ResolvedNtuplizer.cc ThreeJetAnalysis/Resolved/src/ResolvedNtuplizer.cc

 Description: Code to monitor scouting streams.

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  David Sheffield
//      Tweaked by:  Ian Graham, Abhijth Gandrakota
//         Created:  Sat, 14 Nov 2015
//
//

#include "ThreeJetAnalysis/Resolved/interface/ResolvedNtuplizer.h"

using namespace std;
using namespace edm;

//
// constructors and destructor
//

ResolvedNtuplizer::ResolvedNtuplizer(const edm::ParameterSet& iConfig):
    token_jet_pt(consumes<vector<float>>(
                     iConfig.getParameter<InputTag>("jet_pt"))),
    token_jet_eta(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_eta"))),
    token_jet_phi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_phi"))),
    token_jet_E(consumes<vector<float>>(
                    iConfig.getParameter<InputTag>("jet_E"))),
//    token_jet_mass(consumes<vector<float>>(
//                       iConfig.getParameter<InputTag>("jet_mass"))),
    token_jet_CSV(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_csv"))),

//    token_jet_NHE(consumes<vector<float>>(
//                      iConfig.getParameter<InputTag>("jet_NHE"))),
    token_jet_NHEF(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_NHEF"))),
//    token_jet_NEME(consumes<vector<float>>(
//                      iConfig.getParameter<InputTag>("jet_NEME"))),
    token_jet_NEMEF(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_NEMEF"))),
//    token_jet_CHE(consumes<vector<float>>(
//                      iConfig.getParameter<InputTag>("jet_CHE"))),
    token_jet_CHEF(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_CHEF"))),
//    token_jet_CEME(consumes<vector<float>>(
//                      iConfig.getParameter<InputTag>("jet_CEME"))),
    token_jet_CEMEF(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_CEMEF"))),

    token_jet_pt_puppi(consumes<vector<float>>(
                     iConfig.getParameter<InputTag>("jet_pt_puppi"))),
    token_jet_eta_puppi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_eta_puppi"))),
    token_jet_phi_puppi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_phi_puppi"))),
    token_jet_E_puppi(consumes<vector<float>>(
                    iConfig.getParameter<InputTag>("jet_E_puppi"))),
//    token_jet_mass_puppi(consumes<vector<float>>(
//                       iConfig.getParameter<InputTag>("jet_mass_puppi"))),
    token_jet_CSV_puppi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_csv_puppi"))),

//    token_jet_NHE_puppi(consumes<vector<float>>(
//                      iConfig.getParameter<InputTag>("jet_NHE_puppi"))),
    token_jet_NHEF_puppi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_NHEF_puppi"))),
//    token_jet_NEME_puppi(consumes<vector<float>>(
//                      iConfig.getParameter<InputTag>("jet_NEME_puppi"))),
    token_jet_NEMEF_puppi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_NEMEF_puppi"))),
//    token_jet_CHE_puppi(consumes<vector<float>>(
//                      iConfig.getParameter<InputTag>("jet_CHE_puppi"))),
    token_jet_CHEF_puppi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_CHEF_puppi"))),
//    token_jet_CEME_puppi(consumes<vector<float>>(
//                      iConfig.getParameter<InputTag>("jet_CEME_puppi"))),
    token_jet_CEMEF_puppi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("jet_CEMEF_puppi"))),

	token_electron_pt(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("electron_pt"))),
	token_electron_eta(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("electron_eta"))),
	token_electron_phi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("electron_phi"))),

	token_muon_pt(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("muon_pt"))),
	token_muon_eta(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("muon_eta"))),
	token_muon_phi(consumes<vector<float>>(
                      iConfig.getParameter<InputTag>("muon_phi"))),

    token_vertex_z(consumes<vector<float>>(
                       iConfig.getParameter<InputTag>("vertex_z"))),
    token_genpart_ID(consumes<vector<float>>(
                         iConfig.getParameter<InputTag>("genpart_ID"))),
    token_genpart_status(consumes<vector<float>>(
                             iConfig.getParameter<InputTag>("genpart_status"))),
    token_genpart_momID(consumes<vector<float>>(
                            iConfig.getParameter<InputTag>("genpart_momID"))),
    token_genpart_pt(consumes<vector<float>>(
                         iConfig.getParameter<InputTag>("genpart_pt"))),
    token_genpart_eta(consumes<vector<float>>(
                          iConfig.getParameter<InputTag>("genpart_eta"))),
    token_genpart_phi(consumes<vector<float>>(
                          iConfig.getParameter<InputTag>("genpart_phi"))),
    token_genpart_E(consumes<vector<float>>(
						iConfig.getParameter<InputTag>("genpart_E"))),
    token_MET(consumes<vector<float>>(
                        iConfig.getParameter<InputTag>("handle_MET"))),
    token_MET_phi(consumes<vector<float>>(
                        iConfig.getParameter<InputTag>("handle_MET_phi"))),
    cut_nJets_min(iConfig.getParameter<int>("cut_nJets_min")),
    cut_pt(iConfig.getParameter<double>("cut_pt")),
    cut_eta(iConfig.getParameter<double>("cut_eta")),

    file_name(iConfig.getParameter<string>("output_file_name")),
 	is_signal(iConfig.getParameter<bool>("is_signal"))
{
    //now do what ever initialization is needed
    file = new TFile(file_name.c_str(), "RECREATE");

    tree = new TTree("events", "Tree for scouting data");

    tree->Branch("HT", &Ht, "HT/F");

    tree->Branch("jet_num", &jet_num, "jet_num/I");
    tree->Branch("jet_pt", &jet_pt);
    tree->Branch("jet_eta", &jet_eta);
    tree->Branch("jet_phi", &jet_phi);
    tree->Branch("jet_m", &jet_m);
    
    tree->Branch("jet_csv", &jet_csv);
    
//    tree->Branch("jet_NHE", &jet_NHE);
    tree->Branch("jet_NHEF", &jet_NHEF);
//    tree->Branch("jet_NEME", &jet_NEME);
    tree->Branch("jet_NEMEF", &jet_NEMEF);
//    tree->Branch("jet_CHE", &jet_CHE);
    tree->Branch("jet_CHEF", &jet_CHEF);
//    tree->Branch("jet_CEME", &jet_CEME);
    tree->Branch("jet_CEMEF", &jet_CEMEF);

    tree->Branch("vertex_num", &vertex_num, "vertex_num/I");
    //tree->Branch("rho", &rho, "rho/F");
    tree->Branch("Run", &run, "Run/I");
    tree->Branch("Lumi", &lumi, "Lumi/I");
    tree->Branch("Event", &event, "Event/I");

	// lepton-based branches
	tree->Branch("electron_num", &electron_num, "electron_num/I");
	tree->Branch("electron_pt", &electron_pt);
    	tree->Branch("electron_eta", &electron_eta);
    	tree->Branch("electron_phi", &electron_phi);
	tree->Branch("muon_num", &muon_num, "muon_num/I");
	tree->Branch("muon_pt", &muon_pt);
    	tree->Branch("muon_eta", &muon_eta);
    	tree->Branch("muon_phi", &muon_phi);

	tree->Branch("MET", &MET);
	tree->Branch("MET_phi", &MET_phi);
}


ResolvedNtuplizer::~ResolvedNtuplizer()
{

    // do anything here that needs to be done at desctruction time
    // (e.g. close files, deallocate resources etc.)
    file->cd();
    tree->Write();
    file->Close();

}


//
// member functions
//

// ------------ method called for each event  ------------
void
ResolvedNtuplizer::analyze(const edm::Event& iEvent,
                         const edm::EventSetup& iSetup)
{
	cout << " yo" ;
    ResetVariables();
	cout << " yo" ;
    run = iEvent.id().run();
    lumi = iEvent.id().luminosityBlock();
    event = iEvent.id().event();

    int getCollectionsResult = GetCollections(iEvent);
    if (getCollectionsResult)
	return;


	MET = handle_MET->at(0);

	MET_phi = handle_MET_phi->at(0);

    jet_num = jet.size();
    if (jet_num < cut_nJets_min && (signed int)jet_pt_puppi.size() < cut_nJets_min)
        return;

    for (int i=0; i<jet_num; ++i) {
        jet_pt.push_back(jet[i].Pt());
        jet_eta.push_back(jet[i].Eta());
        jet_phi.push_back(jet[i].Phi());
        jet_m.push_back(jet[i].M());
    }

	electron_num = electronPt->size();
    muon_num = muonPt->size();

    for (unsigned i=0; i<electronPt->size(); ++i) {
        electron_pt.push_back(electronPt->at(i));
        electron_eta.push_back(electronEta->at(i));
        electron_phi.push_back(electronPhi->at(i));
    }

    for (unsigned i=0; i<muonPt->size(); ++i) {
        muon_pt.push_back(muonPt->at(i));
        muon_eta.push_back(muonEta->at(i));
        muon_phi.push_back(muonPhi->at(i));
    }

    vertex_num = vertex_z->size();
    //rho = *handle_rho;

	if (is_signal)
		GetGenParticles();

    tree->Fill();

    return;
}


// ------- method called once each job just before starting event loop  -------
void ResolvedNtuplizer::beginJob()
{
}

// ------- method called once each job just after ending the event loop  -------
void ResolvedNtuplizer::endJob()
{
}


// -- method fills 'descriptions' with the allowed parameters for the module  --
void ResolvedNtuplizer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}


void ResolvedNtuplizer::ResetVariables()
{
    jet.clear();

    Ht = 0.0;

    jet_num = 0;
    jet_pt.clear();
    jet_eta.clear();
    jet_phi.clear();
    jet_m.clear();
    jet_csv.clear();
	jet_from_triplet.clear();

//    jet_NHE.clear();
    jet_NHEF.clear();
//    jet_NEME.clear();
    jet_NEMEF.clear();
//    jet_CHE.clear();
    jet_CHEF.clear();
//    jet_CEME.clear();
    jet_CEMEF.clear();

    // leptons
    electron_num = 0;
    electron_pt.clear();
    electron_eta.clear();
    electron_phi.clear();
    muon_num = 0;
    muon_pt.clear();
    muon_eta.clear();
    muon_phi.clear();

    vertex_num = 0;

    MET = 0.0;
    MET_phi = 0.0;

    //rho = 0.0;

    run = 0;
    lumi = 0;
    event = 0;

    return;
}


int ResolvedNtuplizer::GetCollections(const edm::Event& iEvent)
{
    // Get collections from ntuple
    // Returns nonzero if there is a problem getting a collection

    // Get jet variables
    iEvent.getByToken(token_jet_pt, jetPt);
    if (!jetPt.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find jetPt." << endl;
	return 1;
    }

    iEvent.getByToken(token_jet_eta, jetEta);
    if (!jetEta.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find jetEta." << endl;
	return 1;
    }

    iEvent.getByToken(token_jet_phi, jetPhi);
    if (!jetPhi.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find jetPhi." << endl;
	return 1;
    }

    iEvent.getByToken(token_jet_E, jetE);
    if (!jetE.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find jetE." << endl;
	return 1;
    }

//    iEvent.getByToken(token_jet_mass, jetMass);
//    if (!jetMass.isValid()) {
//	throw edm::Exception(edm::errors::ProductNotFound)
//	    << "Could not find jetMass." << endl;
//	return 1;
//    }

    iEvent.getByToken(token_jet_CSV, jetCSV);
    if (!jetCSV.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find jetCSV." << endl;
	return 1;
    }


//    iEvent.getByToken(token_jet_NHE, jetNHE);
//    if (!jetNHE.isValid()) {
//      throw edm::Exception(edm::errors::ProductNotFound)
//	<< "Could not find jetNHE." << endl;
//      return 1;
//    }

    iEvent.getByToken(token_jet_NHEF, jetNHEF);
    if (!jetNHEF.isValid()) {
      throw edm::Exception(edm::errors::ProductNotFound)
	<< "Could not find jetNHEF." << endl;
      return 1;
    }

//    iEvent.getByToken(token_jet_NEME, jetNEME);
//    if (!jetNEME.isValid()) {
//      throw edm::Exception(edm::errors::ProductNotFound)
//	<< "Could not find jetNEME." << endl;
//      return 1;
//    }

    iEvent.getByToken(token_jet_NEMEF, jetNEMEF);
    if (!jetNEMEF.isValid()) {
      throw edm::Exception(edm::errors::ProductNotFound)
	<< "Could not find jetNEMEF." << endl;
      return 1;
    }

/*    iEvent.getByToken(token_jet_CHE, jetCHE);
    if (!jetCHE.isValid()) {
      throw edm::Exception(edm::errors::ProductNotFound)
	<< "Could not find jetCHE." << endl;
      return 1;
    }*/

    iEvent.getByToken(token_jet_CHEF, jetCHEF);
    if (!jetCHEF.isValid()) {
      throw edm::Exception(edm::errors::ProductNotFound)
	<< "Could not find jetCHEF." << endl;
      return 1;
    }

/*    iEvent.getByToken(token_jet_CEME, jetCEME);
    if (!jetCEME.isValid()) {
      throw edm::Exception(edm::errors::ProductNotFound)
	<< "Could not find jetCEME." << endl;
      return 1;
    }*/

    iEvent.getByToken(token_jet_CEMEF, jetCEMEF);
    if (!jetCEMEF.isValid()) {
      throw edm::Exception(edm::errors::ProductNotFound)
	<< "Could not find jetCEMEF." << endl;
      return 1;
    }

    // electron variables (leptons)
    iEvent.getByToken(token_electron_pt, electronPt);
    if (!electronPt.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find electronPt." << endl;
	return 1;
    }

    iEvent.getByToken(token_electron_eta, electronEta);
    if (!electronEta.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find electronEta." << endl;
	return 1;
    }

    iEvent.getByToken(token_electron_phi, electronPhi);
    if (!electronPhi.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find electronPhi." << endl;
	return 1;
    }


    // muons variables
    iEvent.getByToken(token_muon_pt, muonPt);
    if (!muonPt.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find muonPt." << endl;
	return 1;
    }

    iEvent.getByToken(token_muon_eta, muonEta);
    if (!muonEta.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find muonEta." << endl;
	return 1;
    }

    iEvent.getByToken(token_muon_phi, muonPhi);
    if (!muonPhi.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find muonPhi." << endl;
	return 1;
    }

    // Get vertices
    iEvent.getByToken(token_vertex_z, vertex_z);
    if (!vertex_z.isValid()) {
	throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find vertex_z." << endl;
	return 1;
    }

	iEvent.getByToken(token_MET, handle_MET);
        if (!handle_MET.isValid()) {
            throw edm::Exception(edm::errors::ProductNotFound)
                << "Could not find MET." << endl;
            return 1;
        }

	iEvent.getByToken(token_MET_phi, handle_MET_phi);
        if (!handle_MET_phi.isValid()) {
            throw edm::Exception(edm::errors::ProductNotFound)
                << "Could not find MET_phi." << endl;
            return 1;
        }

    // Get rho
    /*iEvent.getByToken(token_rho, handle_rho);
    if (!handle_rho.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find rho." << endl;
	return 1;
        }*/

    for (unsigned i=0; i<jetPt->size(); ++i) {
		
        TLorentzVector tmp_vector;
        tmp_vector.SetPtEtaPhiE(jetPt->at(i), jetEta->at(i), jetPhi->at(i),
                                jetE->at(i));
        if (!JetCuts(tmp_vector)) {
			jet.push_back(tmp_vector);
			jet_csv.push_back(jetCSV->at(i));
	
//			jet_NHE.push_back(jetNHE->at(i));
			jet_NHEF.push_back(jetNHEF->at(i));
//		    jet_NEME.push_back(jetNEME->at(i));
	        jet_NEMEF.push_back(jetNEMEF->at(i));
//	        jet_CHE.push_back(jetCHE->at(i));
	        jet_CHEF.push_back(jetCHEF->at(i));
//	        jet_CEME.push_back(jetCEME->at(i));
		    jet_CEMEF.push_back(jetCEMEF->at(i));
			Ht += jetPt->at(i);
		}
    }
    return 0;
}


void ResolvedNtuplizer::GetGenParticles()
{
    TLorentzVector signal_daughters[6];
    int signal_daughters_matched[6] = {-1, -1, -1, -1, -1, -1};
    int num_to_match = 0;
    for (int unsigned i=0; i<genpart_status->size(); ++i) {
        if (genpart_status->at(i) == 23) {
            int ind = -1;
            if (genpart_momID->at(i) == 6)
                ind = 0;
            else if (genpart_momID->at(i) == 24 && genpart_ID->at(i) > 0)
                ind = 1;
            else if (genpart_momID->at(i) == 24 && genpart_ID->at(i) < 0)
                ind = 2;
            else if (genpart_momID->at(i) == -6)
                ind = 3;
            else if (genpart_momID->at(i) == -24 && genpart_ID->at(i) > 0)
                ind = 4;
            else if (genpart_momID->at(i) == -24 && genpart_ID->at(i) < 0)
                ind = 5;
            else
                continue;

            signal_daughters[ind].SetPtEtaPhiE(genpart_pt->at(i),
                                               genpart_eta->at(i),
                                               genpart_phi->at(i),
                                               genpart_E->at(i));
            signal_daughters_matched[ind] = 0;
            ++num_to_match;
        }
    }

    double match_DeltaR_cut = 0.2;
    double match_pt_cut = 0.25;

    while (num_to_match > 0) {
        double min_DeltaR = 9999.0;
        int min_jet_ind = -1;
        int min_daughter_ind = -1;
        for (int i=0; i<6; ++i) {
            if (signal_daughters_matched[i] != 0)
                continue;

            for (int j=0; j<jet_num; ++j) {
                if (jet_from_triplet[j] != 0)
                    continue;

                double DeltaR = signal_daughters[i].DeltaR(jet[j]);
                if (DeltaR < min_DeltaR) {
                    min_DeltaR = DeltaR;
                    min_daughter_ind = i;
                    min_jet_ind = j;
                }
            }
        }

        signal_daughters_matched[min_daughter_ind] = 1;
        if (min_DeltaR < match_DeltaR_cut
            && fabs(jet[min_jet_ind].Pt()/signal_daughters[min_daughter_ind].Pt()
                    - 1.0) < match_pt_cut) {
            if (min_daughter_ind < 3)
                jet_from_triplet[min_jet_ind] = 1;
            else
                jet_from_triplet[min_jet_ind] = 2;
        }
        --num_to_match;
    }

    return;
}

int ResolvedNtuplizer::JetCuts(const TLorentzVector jet_)
{
    // Returns >0 if jet fails cuts
    // Returns  0 is jet passes cuts
    if (fabs(jet_.Eta()) > cut_eta)
        return 0x1;
    if (jet_.Pt() < cut_pt)
        return 2;

    return 0;
}

//define this as a plug-in
DEFINE_FWK_MODULE(ResolvedNtuplizer);
