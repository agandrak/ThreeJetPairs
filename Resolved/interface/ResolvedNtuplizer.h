// -*- C++ -*-
//
// Package:    ThreeJetAnalysis/Resolved
// Class:      ResolvedNtuplizer
//
/**\class ResolvedNtuplizer ResolvedNtuplizer.h ThreeJetAnalysis/Resolved/interface/ResolvedNtuplizer.h

 Description: Code to monitor scouting streams.

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  David Sheffield
//         Created:  Sat, 14 Nov 2015
//
//


// System include files
#include <memory>
#include <iostream>
#include <vector>
#include <utility>

// CMSSW include files
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"

// Root include files
#include "TLorentzVector.h"
#include "TFile.h"
#include "TTree.h"

// User include files
#include "ThreeJetAnalysis/Utilities/interface/TH1DInitializer.h"
#include "ThreeJetAnalysis/Utilities/interface/TH2DInitializer.h"

//
// class declaration
//

class ResolvedNtuplizer : public edm::EDAnalyzer {
public:
    explicit ResolvedNtuplizer(const edm::ParameterSet&);
    ~ResolvedNtuplizer();

    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);


private:
    virtual void beginJob() override;
    virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
    virtual void endJob() override;
    virtual void ResetVariables();
	virtual void GetGenParticles();
    virtual int GetCollections(const edm::Event&);
    virtual int JetCuts(const TLorentzVector);

    // ----------member data ---------------------------
    edm::EDGetTokenT<std::vector<float>> token_jet_pt;
    edm::EDGetTokenT<std::vector<float>> token_jet_eta;
    edm::EDGetTokenT<std::vector<float>> token_jet_phi;
    edm::EDGetTokenT<std::vector<float>> token_jet_E;
    edm::EDGetTokenT<std::vector<float>> token_jet_mass;
    edm::EDGetTokenT<std::vector<float>> token_jet_CSV;

//	edm::EDGetTokenT<std::vector<float>> token_jet_NHE;
	edm::EDGetTokenT<std::vector<float>> token_jet_NHEF;
//	edm::EDGetTokenT<std::vector<float>> token_jet_NEME;
	edm::EDGetTokenT<std::vector<float>> token_jet_NEMEF;
//	edm::EDGetTokenT<std::vector<float>> token_jet_CHE;
	edm::EDGetTokenT<std::vector<float>> token_jet_CHEF;
//	edm::EDGetTokenT<std::vector<float>> token_jet_CEME;
	edm::EDGetTokenT<std::vector<float>> token_jet_CEMEF;


    edm::EDGetTokenT<std::vector<float>> token_jet_pt_puppi;
    edm::EDGetTokenT<std::vector<float>> token_jet_eta_puppi;
    edm::EDGetTokenT<std::vector<float>> token_jet_phi_puppi;
    edm::EDGetTokenT<std::vector<float>> token_jet_E_puppi;
    edm::EDGetTokenT<std::vector<float>> token_jet_mass_puppi;
    edm::EDGetTokenT<std::vector<float>> token_jet_CSV_puppi;

//	edm::EDGetTokenT<std::vector<float>> token_jet_NHE_puppi;
	edm::EDGetTokenT<std::vector<float>> token_jet_NHEF_puppi;
//	edm::EDGetTokenT<std::vector<float>> token_jet_NEME_puppi;
	edm::EDGetTokenT<std::vector<float>> token_jet_NEMEF_puppi;
//	edm::EDGetTokenT<std::vector<float>> token_jet_CHE_puppi;
	edm::EDGetTokenT<std::vector<float>> token_jet_CHEF_puppi;
//	edm::EDGetTokenT<std::vector<float>> token_jet_CEME_puppi;
	edm::EDGetTokenT<std::vector<float>> token_jet_CEMEF_puppi;


    edm::EDGetTokenT<std::vector<float>> token_electron_pt;
    edm::EDGetTokenT<std::vector<float>> token_electron_eta;
    edm::EDGetTokenT<std::vector<float>> token_electron_phi;

    edm::EDGetTokenT<std::vector<float>> token_muon_pt;
    edm::EDGetTokenT<std::vector<float>> token_muon_eta;
    edm::EDGetTokenT<std::vector<float>> token_muon_phi;

    edm::EDGetTokenT<std::vector<float>> token_vertex_z;
    edm::EDGetTokenT<std::vector<float>> token_genpart_ID;
    edm::EDGetTokenT<std::vector<float>> token_genpart_status;
    edm::EDGetTokenT<std::vector<float>> token_genpart_momID;
    edm::EDGetTokenT<std::vector<float>> token_genpart_pt;
    edm::EDGetTokenT<std::vector<float>> token_genpart_eta;
    edm::EDGetTokenT<std::vector<float>> token_genpart_phi;
	edm::EDGetTokenT<std::vector<float>> token_genpart_E;
    
    edm::EDGetTokenT<std::vector<float>> token_MET;
    edm::EDGetTokenT<std::vector<float>> token_MET_phi;
    

    edm::Handle<std::vector<float>> jetPt;
    edm::Handle<std::vector<float>> jetEta;
    edm::Handle<std::vector<float>> jetPhi;
    edm::Handle<std::vector<float>> jetE;
    edm::Handle<std::vector<float>> jetMass;
    edm::Handle<std::vector<float>> jetCSV;

//	edm::Handle<std::vector<float>> jetNHE;
	edm::Handle<std::vector<float>> jetNHEF;
//	edm::Handle<std::vector<float>> jetNEME;
	edm::Handle<std::vector<float>> jetNEMEF;
//	edm::Handle<std::vector<float>> jetCHE;
	edm::Handle<std::vector<float>> jetCHEF;
//	edm::Handle<std::vector<float>> jetCEME;
	edm::Handle<std::vector<float>> jetCEMEF;


    edm::Handle<std::vector<float>> jetPtPuppi;
    edm::Handle<std::vector<float>> jetEtaPuppi;
    edm::Handle<std::vector<float>> jetPhiPuppi;
    edm::Handle<std::vector<float>> jetEPuppi;
    edm::Handle<std::vector<float>> jetMassPuppi;
    edm::Handle<std::vector<float>> jetCSVPuppi;

//	edm::Handle<std::vector<float>> jetNHEPuppi;
	edm::Handle<std::vector<float>> jetNHEFPuppi;
//	edm::Handle<std::vector<float>> jetNEMEPuppi;
	edm::Handle<std::vector<float>> jetNEMEFPuppi;
//	edm::Handle<std::vector<float>> jetCHEPuppi;
	edm::Handle<std::vector<float>> jetCHEFPuppi;
//	edm::Handle<std::vector<float>> jetCEMEPuppi;
	edm::Handle<std::vector<float>> jetCEMEFPuppi;


    edm::Handle<std::vector<float>> electronPt;
    edm::Handle<std::vector<float>> electronEta;
    edm::Handle<std::vector<float>> electronPhi;

    edm::Handle<std::vector<float>> muonPt;
    edm::Handle<std::vector<float>> muonEta;
    edm::Handle<std::vector<float>> muonPhi;

    edm::Handle<std::vector<float>> vertex_z;
    edm::Handle<std::vector<float>> genpart_ID;
    edm::Handle<std::vector<float>> genpart_status;
    edm::Handle<std::vector<float>> genpart_momID;
    edm::Handle<std::vector<float>> genpart_pt;
    edm::Handle<std::vector<float>> genpart_eta;
    edm::Handle<std::vector<float>> genpart_phi;
	edm::Handle<std::vector<float>> genpart_E;
    //edm::Handle<double> handle_rho;

    edm::Handle<std::vector<float>> handle_MET;
    edm::Handle<std::vector<float>> handle_MET_phi;

	float MET_cut;

    std::vector<TLorentzVector> jet;

    int cut_nJets_min;
    double cut_pt;
    double cut_eta;
    // leptons

    std::string file_name;
    bool is_signal;

    TFile *file;
    TTree *tree;

    float Ht;

    int jet_num;
    std::vector<float> jet_pt;
    std::vector<float> jet_eta;

    std::vector<float> jet_pt_puppi;
    std::vector<float> jet_eta_puppi;


	std::vector<float> jet_NHE;
	std::vector<float> jet_CHE;
	std::vector<float> jet_NHEF;
	std::vector<float> jet_CHEF;
	std::vector<float> jet_NEME;
	std::vector<float> jet_CEME;
	std::vector<float> jet_NEMEF;
	std::vector<float> jet_CEMEF;
    std::vector<float> jet_phi;
    std::vector<float> jet_m;
    std::vector<float> jet_csv;
	std::vector<int> jet_from_triplet;

    //for leptons
    int electron_num;
    std::vector<float> electron_pt;
    std::vector<float> electron_eta;
    std::vector<float> electron_phi;
    int muon_num;
    std::vector<float> muon_pt;
    std::vector<float> muon_eta;
    std::vector<float> muon_phi;

    int vertex_num;


    float MET;
    float MET_phi;

    //float rho;

    int run;
    int lumi;
    int event;
};
