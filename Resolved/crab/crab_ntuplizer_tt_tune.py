from CRABClient.UserUtilities import config
config = config()

#dataset = 'JetHT'
#version = 'v2'

dataset = 'TT_Tune'
version = 'v2'

config.General.requestName = 'Resolved_Ntuples_{0}_{1}'.format(dataset, version)
config.General.workArea = 'jobs'

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = '../python/ResolvedNtuplizer_cfg.py'
output_name = 'resolved_ntuple_{0}_{1}.root'.format(dataset, version)
config.JobType.pyCfgParams = ['outputFile={0}'.format(output_name)]
config.JobType.outputFiles = [output_name]
#config.Data.inputDataset = '/JetHT/asparker-RunIISpring16MiniAODv2_B2GAnaFW_80x_V1p0-c9ad27f972ae59d36cd924fb5f87408c/USER'

#config.Data.inputDataset = '/TT_TuneCUETP8M1_13TeV-powheg-scaleup-pythia8/knash-RunIISpring16MiniAODv2_B2GAnaFW_80x_V1p0_v2-4e74e3854bbd13b3866f4a57304f402f/USER'
config.Data.inputDataset = '/TT_TuneCUETP8M1_13TeV-powheg-pythia8/grauco-B2GAnaFW_80X_V2p1-edbed0685401a5848e7d61871b3a63d8/USER'
config.Data.inputDBS = 'phys03'
config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 5000
#NJOBS = 264
#config.Data.totalUnits = config.Data.unitsPerJob * NJOBS
#config.Data.totalUnits = 99469
config.Data.publication = False
config.Data.ignoreLocality = True

config.Site.storageSite = 'T3_US_FNALLPC'
