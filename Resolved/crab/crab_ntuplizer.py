from CRABClient.UserUtilities import config
config = config()

#dataset = 'JetHT'
#version = 'v2'

dataset = 'TT_Tune'
version = 'v1'

config.General.requestName = 'Resolved_Ntuples_{0}_{1}'.format(dataset, version)
config.General.workArea = 'jobs'

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = '../python/ResolvedNtuplizer_cfg.py'
output_name = 'resolved_ntuple_{0}_{1}.root'.format(dataset, version)
config.JobType.pyCfgParams = ['outputFile={0}'.format(output_name)]
config.JobType.outputFiles = [output_name]
#config.Data.inputDataset = '/JetHT/asparker-RunIISpring16MiniAODv2_B2GAnaFW_80x_V1p0-c9ad27f972ae59d36cd924fb5f87408c/USER'

config.Data.inputDataset = '/TT_TuneCUETP8M1_13TeV-powheg-pythia8/srappocc-RunIISpring16MiniAODv2_B2GAnaFW_80x_V1p0-4e74e3854bbd13b3866f4a57304f402f/USER'
config.Data.inputDBS = 'phys03'
config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 100
#NJOBS = 264
#config.Data.totalUnits = config.Data.unitsPerJob * NJOBS
config.Data.totalUnits = 104393
config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Cert_271036-277148_13TeV_PromptReco_Collisions16_JSON.txt'
config.Data.publication = False
config.Data.ignoreLocality = True

config.Site.storageSite = 'T3_US_FNALLPC'
