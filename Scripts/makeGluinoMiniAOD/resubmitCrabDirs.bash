#!/bin/bash

ls crab_glu_* | grep : | cut -d: -f1 > crab_dirs.txt

num=`wc -l crab_dirs.txt | cut -d" " -f1`

for i in `seq 1 $num`; do
	crab resubmit `sed "${i}q;d" crab_dirs.txt`
done

rm crab_dirs.txt
