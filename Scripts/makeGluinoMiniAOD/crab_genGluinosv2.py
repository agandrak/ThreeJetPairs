from CRABClient.UserUtilities import config
config = config()

fileBase = "/cms/abhijith/data22/mc_gen/Madgraph/Results_v2/LHE2/"

inputFileTag = "glu_2000_4"
eventsPerJob = 100
totalEvents = 100000

config.General.workArea = 'jobs'
config.General.requestName = inputFileTag


inputFileName = inputFileTag + '.lhe'
inputFileNameGZ = inputFileName + '.gz'
inputFileArg = '--inputFile=' + inputFileName

eventsPerJobArg = '--eventsPerJob=' + str(eventsPerJob)

inputFileBaseNameGZ = fileBase + inputFileNameGZ

config.JobType.pluginName = 'PrivateMC'
config.JobType.generator = 'lhe'
config.JobType.psetName = 'dummy_cfg.py'
config.JobType.scriptExe = 'genGluinosv2.bash'
config.JobType.scriptArgs = [inputFileArg, eventsPerJobArg]
config.JobType.disableAutomaticOutputCollection = True
config.JobType.inputFiles = [inputFileBaseNameGZ]
config.JobType.outputFiles = ["MiniAODv2.root"]

config.Data.outputPrimaryDataset = 'gg-6j_13T-MG_v2'
config.Data.splitting = 'EventBased'
config.Data.unitsPerJob = eventsPerJob
config.Data.totalUnits = totalEvents
config.Data.publication = False
config.Data.ignoreLocality = True
config.Data.outputDatasetTag = config.General.requestName
#config.Data.outLFNDirBase = '/store/group/lpcrutgers/igraham'

config.Site.storageSite = 'T3_US_FNALLPC'
config.Site.blacklist = ['T2_US_Vanderbilt', 'T3_US_Colorado', 'T3_US_PuertoRico'] # have been found to cause problems
