//#include "GluinoEff.h"
//#include "src/ThreeJetAnalysis/Utilities/interface/TH2DInitializer.h"
using namespace std;

//void gluinoEff(char* background_file, char* gluino_file){

// apparently this isn't defined in Root?
long Istoi(const char *s)
{
    long i;
    i = 0;
    while(*s >= '0' && *s <= '9')
    {
        i = i * 10 + (*s - '0');
        s++;
    }
    return i;
}

void SetBinErrorsZero(TH1D* &h) {
	int n_bins = h->GetXaxis()->GetNbins();
	for (int i=1; i <= n_bins; i++) {
		h->SetBinError(i, 0.0);
	}
}

void SetMax(int &maxQuant, int &maxSrB, int quant, double SrB){
	if (SrB > maxSrB) {
		maxSrB = SrB;
		maxQuant = quant;
	}
}

void gluinoEff(){

	const char* background_file = "comb_QCD.root";
	
	TFile *bkg_file = TFile::Open(background_file);
	
	int masses[20] = {100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900};
	string glu_file_pre = "correct_histograms_GluinoJets_";

	TString t_name = "Gluino_Optimization.root";
	TFile *out_file = new TFile(t_name, "RECREATE");
	TDirectory* dir_masses[20]

	int maxJetNum = 0;
	int maxHt = 0;
	int maxPt = 0;
	double maxSoverRootB = 0.0;
	for (int k = 0; k < 12; k++) {
		
		string *gluino_file = new string(glu_file_pre);
		gluino_file->append(to_string(masses[k]));
		gluino_file->append(".root");
		const char* c_gluino_file = gluino_file->c_str();

		TFile *glu_file;

		try{
			glu_file = TFile::Open(c_gluino_file);

			if (glu_file->IsZombie()) continue;
			cout << "break!" << endl;
		}catch(...){
			cout << "Err: File " << c_gluino_file << " does not exist." << endl;
			continue;
		}
		string tmp = *gluino_file;

		string name = tmp.substr(0, tmp.find("."));	

		string mass = name.substr(name.find("GluinoJets_") + 11, name.size());
		int mass_n = Istoi(mass.c_str());	

		string *histogram_name = new string("h_DeltaMassDistanceCut_Eff_");
		histogram_name->append(to_string(masses[k]));
		
        const char* str = histogram_name->c_str();

		TH2D* h2_eff = new TH2D(str, str, 19, .0375, 0.5125 , 31, -5, 305);
		h2_eff->GetXaxis()->SetTitle("Normalized Mass Distance Squared Cut");
		h2_eff->GetYaxis()->SetTitle("Delta Cut");


		const char* dir_name = mass.c_str();	
		
		dir_masses[k] = out_file->mkdir(dir_name)
		dir_masses[k]->cd();
		for (int i = 0; i <= 300; i=i+10) {
			string* token = new string("Delta_Cuts/h_M_DeltaCut_");
			string num = to_string(i);
			token->append(num);
			const char* c_token = token->c_str();

			TH1D* tmp_bkg = (TH1D*)bkg_file->Get(c_token);
			TH1D* tmp_signal = (TH1D*)glu_file->Get(c_token);

			TAxis *axis = tmp_bkg->GetXaxis();

			int bmin = axis->FindBin(mass_n - 10);
			int bmax = axis->FindBin(mass_n + 10);
			double B = tmp_bkg->Integral(bmin, bmax);
			double S = tmp_signal->Integral(bmin, bmax);
			double out = S / sqrt(B);

//			h_efficiency->Fill(i,out);

			for (int j = 0; j < 19; j++) {
				string* token2 = new string("Mass_Distance_Cuts/DistanceCut_0.");
				string dal = to_string(500 - 25*j);
				double dal_n = (500 - 25*j)/1000.0;
				if (dal_n < 0.100) dal.insert(0,"0");
				token2->append(dal);
				token2->append("/h_M_MassDistanceCut_0.");
				token2->append(dal);
				token2->append("_DeltaCut_");
				token2->append(num);
	
				c_token = token2->c_str();
				cout << c_token << endl;
				TH1D* tmp2_bkg = (TH1D*)bkg_file->Get(c_token);
				TH1D* tmp2_signal = (TH1D*)glu_file->Get(c_token);
				B = tmp2_bkg->Integral(bmin, bmax);
				S = tmp2_signal->Integral(bmin, bmax);
				out = S / sqrt(B);
				h2_eff->Fill(dal_n, i, out);
			}
		}
/*        char *str2[15];
        sprintf(str2, "Mass_", to_string(masses[k]));
		dir_masses[k] = outfile->mkdir(str2);*/
//		out_file->cd();
//		SetBinErrorsZero(h_efficiency);
//		h_efficiency->Write();
		h2_eff->Write();
		for (int l = 30; l <=50; l = l + 10) {
			string* glu_pt_file_pre = new string("correct_histograms_Pt")
			string glu_pt_file_post = "_GluinoJets_";
			TH2D* h2_pt_eff = (TH2D*)h2_eff->Clone();
			h2_pt_eff->Reset();
			glu_pt_file_pre->append(to_string(l));
			glu_pt_file_pre->append(glu_pt_file_post);
			glu_pt_file_pre->append(masses[k]);
			glu_pt_file_pre->append(".root");
			TFile* glu_pt_file = TFile::Open((const char*)glu_pt_file_pre->c_str());
			for (int i = 0; i <= 300; i=i+10) {
				string* token = new string("Delta_Cuts/h_M_DeltaCut_");
				string num = to_string(i);
				token->append(num);
				const char* c_token = token->c_str();
	
				TH1D* tmp_bkg = (TH1D*)bkg_file->Get(c_token);
				TH1D* tmp_signal = (TH1D*)glu_pt_file->Get(c_token);
	
				TAxis *axis = tmp_bkg->GetXaxis();
	
				int bmin = axis->FindBin(mass_n - 10);
				int bmax = axis->FindBin(mass_n + 10);
				double B = tmp_bkg->Integral(bmin, bmax);
				double S = tmp_signal->Integral(bmin, bmax);
				double out = S / sqrt(B);
	
	//			h_efficiency->Fill(i,out);
	
				for (int j = 0; j < 19; j++) {
					string* token2 = new string("Mass_Distance_Cuts/DistanceCut_0.");
					string dal = to_string(500 - 25*j);
					double dal_n = (500 - 25*j)/1000.0;
					if (dal_n < 0.100) dal.insert(0,"0");
					token2->append(dal);
					token2->append("/h_M_MassDistanceCut_0.");
					token2->append(dal);
					token2->append("_DeltaCut_");
					token2->append(num);
	
					c_token = token2->c_str();
					cout << c_token << endl;
					TH1D* tmp2_bkg = (TH1D*)bkg_file->Get(c_token);
					TH1D* tmp2_signal = (TH1D*)glu_file->Get(c_token);
					B = tmp2_bkg->Integral(bmin, bmax);
					S = tmp2_signal->Integral(bmin, bmax);
					out = S / sqrt(B);
					h2_eff->Fill(dal_n, i, out);
				}
			}	
		}
	}
	TFile *ttbar_file = TFile::Open("histograms_TTJets.root");
	

	out_file->Close();

}

