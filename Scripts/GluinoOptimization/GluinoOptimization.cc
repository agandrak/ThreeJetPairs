#include "GluinoOptimization.h"

using namespace std;

const int mass_array_size = 19;
const int HT_array_size = 8;
const int PT_array_size = 7;
const int NJ_array_size = 3;
const int masses[mass_array_size] = {100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900};
const int HTCuts[HT_array_size] = {410, 450, 500, 550, 750, 800, 850, 900};
const int PTCuts[PT_array_size] = {20, 25, 30, 35, 40, 45, 50};
const int NJCuts[NJ_array_size] = {5, 6, 7};

const TString s_us = "_"; 

// MUST EXECTURE GluinoOptimization.cc IN THE GluinoCutHistograms 

long Istoi(const char *s)
{
    long i;
    i = 0;
    while(*s >= '0' && *s <= '9')
    {
        i = i * 10 + (*s - '0');
        s++;
    }
    return i;
}

void SetMaxArray(double (&maxSrB)[mass_array_size][4], array<double,4> SrB, int index){
	if (SrB[0] > maxSrB[index][0]) {
		for (int i = 0; i < 4; i++) {
			maxSrB[index][i] = SrB[i];
		}
	}
}

void SetMax(double &maxSrB, double SrB){
	if (SrB > maxSrB) {
		maxSrB = SrB;
	}
}

template< typename T, size_t N, size_t M >
void printArray( T(&theArray)[N][M]  ) {
    for ( int x = 0; x < N; x ++ ) {
		cout << masses[x] << ": ";
        for ( int y = 0; y < M; y++ ) {
            cout << theArray[x][y] << " ";
        }
		cout << endl;
    }
}
//TODO add incorrect gluino histogram to bkg estimation
TH2D* OptimizationPlot(TFile* bkg_file, TFile* glu_file, int mass_n, TString ht_s, TString pt_s, TString nj_s, double &maxSrB){

	string *histogram_name = new string("h_DeltaMassDistanceCut_Eff_");
	histogram_name->append(to_string(mass_n));
	histogram_name->append("GeV");

	TString histogram_name2 = "Njet>=" + nj_s + " HT>" + ht_s + " pT>" + pt_s + " Efficiency";

	double SrB = 0;
		
	const char* str = histogram_name->c_str();

	TH2D* h2_eff = new TH2D(str, histogram_name2, 19, .0375, 0.5125 , 31, -5, 305);
	h2_eff->GetXaxis()->SetTitle("Normalized Mass Distance Squared Cut");
	h2_eff->GetYaxis()->SetTitle("Delta Cut");

	int bmin;
	int bmax;
	double B;
	double S;
	double out;

	for (int i = 0; i <= 300; i=i+10) {
		string num = to_string(i);
		for (int j = 0; j < 19; j++) {
			string* token2 = new string("Mass_Distance_Cuts/DistanceCut_0.");
			string dal = to_string(500 - 25*j);
			double dal_n = (500 - 25*j)/1000.0;
			if (dal_n < 0.100) dal.insert(0,"0");
			token2->append(dal);
			token2->append("/h_M_MassDistanceCut_0.");
			token2->append(dal);
			token2->append("_DeltaCut_");
			token2->append(num);

			const char* c_token = token2->c_str();
			cout << c_token << endl;
			TH1D* tmp2_bkg = (TH1D*)bkg_file->Get(c_token);
			TH1D* tmp2_signal = (TH1D*)glu_file->Get(c_token);

			TAxis *axis = tmp2_bkg->GetXaxis();
			bmin = axis->FindBin(mass_n - 25);
			bmax = axis->FindBin(mass_n + 25);

			B = tmp2_bkg->Integral(bmin, bmax);
			S = tmp2_signal->Integral(bmin, bmax);
			SrB = S / sqrt(B);
			h2_eff->Fill(dal_n, i, SrB);
			SetMax(maxSrB, SrB);
		}
	}
	return h2_eff;
}

void InitializeDiagnosticPlots() {
	
}

void GluinoOptimization() {

	TFile *glu_file;
	TFile *bkg_file;

	TString outfile_name = "Gluino_Optimization.root";
	TFile *out_file = new TFile(outfile_name, "RECREATE");

	TString s_NJ;
	TString s_PT;
	TString s_HT;

	int n_NJ;
	int n_PT;
	int n_HT;

	TString nj_dir = "NJet_";
	TString ht_dir = "HT_";
	TString pt_dir = "Pt_";

	TDirectory* dirs_NJ[NJ_array_size];

	double maxSrB[mass_array_size][4] = {0};

	TDirectory* dir_diagnostics = out_file->mkdir("Diagnostics");

	for (int i = 0; i < NJ_array_size; i++) {
		TDirectory* dirs_HT[HT_array_size];
		s_NJ = to_string(NJCuts[i]);
		n_NJ = NJCuts[i];
		dirs_NJ[i] = out_file->mkdir(nj_dir + s_NJ);
		for (int j = 0; j < HT_array_size; j++) {
			TDirectory* dirs_PT[PT_array_size];
			s_HT = to_string(HTCuts[j]);
			n_HT = HTCuts[j];
			dirs_HT[j] = dirs_NJ[i]->mkdir(ht_dir + s_HT);
			for (int k = 0; k < PT_array_size; k++) {
				s_PT = to_string(PTCuts[k]);
				n_PT = PTCuts[k];
				dirs_PT[k] = dirs_HT[j]->mkdir(pt_dir + s_PT);
				dirs_PT[k]->cd();
				TString s_file_pre = s_HT + s_us + s_PT + s_us + s_NJ + s_us;
//				cout << s_file_pre << endl;
//				if (s_file_pre.EqualTo(TString("550_25_6_"))) continue; //BECAUSE ONE OF THE SETS OF FILES IS MISSING CURRENTLY

				TString s_bkg = "CombinedHistograms/" + s_file_pre + "0.root";
				bkg_file = TFile::Open(s_bkg);
			
				if (bkg_file == NULL) {
					cout << "File " + s_bkg + " not found." << endl;
					bkg_file->Close();
					continue;
				}
				else {
					cout << "File " + s_bkg + " found!" << endl;
				}
				
				for (int l = 0; l < mass_array_size; l++) {
					TString s_glu = "correct_histograms" + s_file_pre + "1_GluinoJets_" + to_string(masses[l]) + ".root";
					glu_file = TFile::Open(s_glu);

					if (glu_file == NULL) {
						cout << "File " + s_glu + " not found." << endl;
						glu_file->Close();
						continue;
					}
					else {
						cout << "File " + s_glu + " found!" << endl;
					}
					
					double SrB = 0;
					TH2D* h2 = OptimizationPlot(bkg_file, glu_file, masses[l], s_NJ, s_HT, s_PT, SrB);
					array<double,4> a_SrB = {SrB, (double) n_HT, (double) n_PT, (double) n_NJ};
					SetMaxArray(maxSrB, a_SrB, l);
					dirs_PT[k]->cd();
					h2->Write();
					glu_file->Close();
				}
				bkg_file->Close();
			}
		}
	}
	out_file->Close();

	printArray(maxSrB);
}
