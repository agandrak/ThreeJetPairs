universe = vanilla
initialdir = .
error = output/error/submitjobs$(Process).error
log = output/log/submitjobs$(Process).log
output = output/out/submitjobs$(Process).out
executable = haddQCDHistograms.bash
arguments = $(Process)
Notification=never
queue 200
