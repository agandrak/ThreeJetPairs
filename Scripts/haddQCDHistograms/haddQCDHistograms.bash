#!/bin/bash

### NEED to get CMSSW software working with condor
source /osg/osg3.2/osg-wn-client/setup.sh
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh
export SCRAM_ARCH=slc6_amd64_gcc530
export CMSSW_GIT_REFERENCE=/cvmfs/cms.cern.ch/cmssw.git
cd ../../../GluinoCutHistograms/
eval `scramv1 runtime -sh`
export HOME=/users/h2/graham/
###

count=0

HTcuts="410 450 500 550 750 800 850 900"
PTcuts="20 25 30 35 40 45 50"
NJcuts="5 6 7"
isGluino="0 1"

for i in $HTcuts; do
	for j in $PTcuts; do
		for k in $NJcuts; do
			if (($count==$1)); then
				tag=`ls | grep ${i}_${j}_${k}_0`
				echo $tag
				hadd -f CombinedHistograms/${i}_${j}_${k}_0.root $tag
				exit
			else
				count=$((count+1))
			fi				
		done
	done
done

echo "Loop never ended"
exit
