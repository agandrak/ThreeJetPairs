#!/bin/bash

### NEED to get CMSSW software working with condor
source /osg/osg3.2/osg-wn-client/setup.sh
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch
source $VO_CMS_SW_DIR/cmsset_default.sh
export SCRAM_ARCH=slc6_amd64_gcc530
export CMSSW_GIT_REFERENCE=/cvmfs/cms.cern.ch/cmssw.git
cd ../../..
eval `scramv1 runtime -sh`
export HOME=/users/h2/graham/
###

#pwd

scriptpath=`find -name remakeHistos.py`

string=`sed "$(($1+1))q;d" GluinoCutHistograms/missingFiles.txt`

#echo $string

#[[ $string =~ ^(_*)(.*[^_])(_*)$ ]] #replace underscores with white space

argument=`echo $string | perl -pe 's/(?:^_+|_+$)(*SKIP)(*F)|_/ /g'`

#echo $argument

echo $scriptpath $argument
python $scriptpath $argument

exit
