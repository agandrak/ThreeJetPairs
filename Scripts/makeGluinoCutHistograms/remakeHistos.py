#!/usr/bin/python

import os
import sys
import subprocess

x = int(sys.argv[1])
y = int(sys.argv[2])
z = int(sys.argv[3])
w = int(sys.argv[4])

sample = ""
if w == 1:
	sample = "gluino"
else:
	sample = "qcdHt"

mkdir = "mkdir GluinoCutHistograms; "

cd = "cd GluinoCutHistograms; "

command = "python ../ThreeJetAnalysis/Scouting/bin/makeScoutingHistograms.py "

command = command + " HtMin=" + str(x) + " Pt=" + str(y) + " cutNJetsMin=" + str(z) + " do=" + sample + " outputFile=histograms" + str(x) + "_" + str(y) + "_" + str(z) + "_" + str(w) + ".root" 

print command

print "Running: " + command  
os.system(cd + command)
