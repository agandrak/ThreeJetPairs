#!/usr/bin/python

import os
import sys
import subprocess

argument = int(sys.argv[1])
str_arg = str(argument)

HTcuts = [410,450,500,550,750,800,850,900]
PTcuts = [20,25,30,35,40,45,50,55,60,65,70,75,80,85,90]
NJcuts = [5,6,7,8,9,10,11,12]
isGluino = [0,1]

count = 0;

print argument

x = -1
y = -1
z = -1
w = -1

doBreak = False
'''
for i in range(0,8):
	for j in range(0,7):
		for k in range(0,3):
			for l in range(0,2):
				if count == argument:
					x = HTcuts[i]
					y = PTcuts[j]
					z = NJcuts[k]
					w = isGluino[l]
					doBreak = True
					break
				else:
					count += 1
			if doBreak:
				break
		if doBreak:
			break
	if doBreak:
		break
'''
# LOOP FOR MAKING ADDITIONAL CUT PLOTS
for i in range(0,8):
	for j in range(0,11):
		for k in range(0,6):
			for l in range(0,2):
				if j < 7 and k < 3:
					continue
				if count == argument:
					x = HTcuts[i]
					y = PTcuts[j]
					z = NJcuts[k]
					w = isGluino[l]
					doBreak = True
					break
				else:
					count += 1
			if doBreak:
				break
		if doBreak:
			break
	if doBreak:
		break

# LOOP FOR MAKING ADDITIONAL CUT PLOTS


if x == -1 and y == -1 and z == -1 and w == -1:
	print "Argument is too high. Exiting."
	quit()

sample = ""
if w == 1:
	sample = "gluino"
else:
	sample = "qcdHt"

mkdir = "mkdir GluinoCutHistograms; "

cd = "cd GluinoCutHistograms; "

command = "python ../ThreeJetAnalysis/Scouting/bin/makeScoutingHistograms.py "

command = command + " HtMin=" + str(x) + " Pt=" + str(y) + " cutNJetsMin=" + str(z) + " do=" + sample + " outputFile=histograms" + str(x) + "_" + str(y) + "_" + str(z) + "_" + str(w) + ".root" 

print command

print "Running: " + command  
os.system(cd + command)
