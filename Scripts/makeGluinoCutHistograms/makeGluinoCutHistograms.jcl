universe = vanilla
initialdir = .
error = output/error/submitjobs$(Process).error
log = output/log/submitjobs$(Process).log
output = output/out/submitjobs$(Process).out
executable = makeGluinoCutHistograms.bash
arguments = $(Process)
Notification=never
queue 721
