#!/bin/bash

# Run this after "makeCutHistograms" has been completed on Condor

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $DIR
cd ../../..
cd GluinoCutHistograms

HTcuts="410 450 500 550 750 800 850 900"
PTcuts="20 25 30 35 40 45 50"
NJcuts="5 6 7"
isGluino="0 1"
pwd

for i in $HTcuts; do
	for j in $PTcuts; do
		for k in $NJcuts; do
			tag=`ls | grep ${i}_${j}_${k}_0`
			echo $tag
			hadd CombinedHistograms/TESTHADD_${i}_${j}_${k}_0.root $tag
			sleep 5
		done
	done
done
