GluinoToJets Code
=======================

Author: Abhijith Gandrakota & Ian Graham

Analysis code for RPV gluinos decaying to jets.

Use it in CMSSW_8_0_22

Subpackages
-----------

* **Resolved** analysis as well as RECO MC for scouting analysis.
* **Scouting** analysis.
* **Utilities** used by other parts of the code.
* **Generation** of signal with matrix element generators.
* **Simulation** of MC.
* **MonitorScouting** datasets and validation.
