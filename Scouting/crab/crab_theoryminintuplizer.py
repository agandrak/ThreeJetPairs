from CRABClient.UserUtilities import config
config = config()

letter="B"
version="2"

config.General.requestName = 'MuonScouting_Ntuples_Run2016'+letter+'_v'+version

config.General.workArea = 'jobs'

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = '../python/TheoryMiniTuplizer_cfg.py'
#config.Data.outputPrimaryDataset = 'TheoryMini'
#config.Data.outputDatasetTag = 'MiniTuples_MuonPhysJSON_Run2016'+letter+'_v'+version

config.JobType.outputFiles = ['theory_minituple.root']

config.Data.inputDataset = '/ScoutingPFMuons/Run2016'+letter+'-v'+version+'/RAW' # entire scouting dataset
#config.Data.inputDataset = '/ScoutingPFHT/Run2015D-v1/RAW' # entire scouting dataset
config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 200
#NJOBS = 190
#config.Data.totalUnits = config.Data.unitsPerJob * NJOBS
config.Data.totalUnits = 63496
#config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Cert_271036-275125_13TeV_PromptReco_Collisions16_JSON.txt' 
config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Cert_271036-282092_13TeV_PromptReco_Collisions16_JSON_MuonPhys.txt'
config.Data.publication = False
config.Data.ignoreLocality = True

config.Site.storageSite = 'T3_US_FNALLPC'
