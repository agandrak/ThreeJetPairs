from CRABClient.UserUtilities import config
config = config()

letter="F"
version="1"

config.General.requestName = 'TEST2_PFScouting_Ntuples_LowPU_Run2016'+letter+'_v'+version
#config.General.requestName = 'PFCommissioning_Ntuples_Run2016'+letter+'_v'+version

config.General.workArea = 'jobs'

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = '../python/ScoutingNtuplizer_cfg.py'
config.JobType.outputFiles = ['scouting_ntuple.root']

config.Data.inputDataset = '/ScoutingPFHT/Run2016'+letter+'-v'+version+'/RAW' # entire scouting dataset
#config.Data.inputDataset = '/ScoutingPFHT/Run2015D-v1/RAW' # entire scouting dataset
config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 164
#NJOBS = 190
#config.Data.totalUnits = config.Data.unitsPerJob * NJOBS
#config.Data.totalUnits = 27623
#config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Cert_271036-275125_13TeV_PromptReco_Collisions16_JSON.txt' 
config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Final/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON.txt'
#lowPU json
#config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON_LowPU.txt'
#lowlowPU json
#config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON_LowLowPU.txt'
config.Data.publication = False
config.Data.ignoreLocality = True

config.Site.storageSite = 'T3_US_FNALLPC'
