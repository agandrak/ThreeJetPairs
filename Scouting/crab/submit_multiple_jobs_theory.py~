##################################################################
########   TO RUN THIS: python submit_multiple_jobs.py
########   DO NOT DO: crab submit submit_multiple_jobs.py
########
########   From https://github.com/alefisico/RUNA
##################################################################

from CRABClient.UserUtilities import config
from httplib import HTTPException
from multiprocessing import Process
from CRABAPI.RawCommand import crabCommand

def submit(config):
    try:
        crabCommand('submit', config = config)
    except HTTPException, hte:
        print 'Cannot execute command'
        print hte.headers

if __name__ == '__main__':
    Samples = [
        '/ScoutingPFMuons/Run2016B-v2/RAW',
        '/ScoutingPFMuons/Run2016C-v2/RAW',
		'/ScoutingPFMuons/Run2016D-v2/RAW',
		'/ScoutingPFMuons/Run2016E-v2/RAW',
		'/ScoutingPFMuons/Run2016B-v1/RAW',
		'/ScoutingPFMuons/Run2016F-v1/RAW',
		'/ScoutingPFMuons/Run2016G-v1/RAW',
		'/ScoutingPFMuons/Run2016H-v1/RAW'
        ]

    config = config()

    version = 'v1'

    config.General.requestName = ''
    config.General.workArea = 'jobs'

    config.JobType.pluginName = 'Analysis'
    config.JobType.psetName = '../python/TheoryMiniTuplizer_cfg.py'
    config.JobType.allowUndistributedCMSSW = True

    config.Data.inputDataset = ''
    config.Data.splitting = 'LumiBased'
    config.Data.unitsPerJob = 500 # to be determined, though 500 sounds like a good number for ScoutingPFMuons
    config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Final/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON_MuonPhys.txt'
    config.Data.publication = False
    config.Data.ignoreLocality = True

    config.Data.outLFNDirBase = "/store/group/lpcrutgers/igraham"

    config.Site.storageSite = 'T3_US_FNALLPC'


    for dataset in Samples:
        dataset_string = dataset.split('/ScoutingPFMuons/')[1].split('/RAW')[0]
        dataset_name = dataset_string.split('-')[0]
        version = dataset_string.split('-')[1]
        name = 'Theory_Muon_Ntuples_{0}_{1}'.format(dataset_name, version)
        config.General.requestName = name
        config.Data.outputDatasetTag = name
        output_name = 'theory_ntuple_{0}_{1}.root'.format(dataset_name,
                                                            version)
        config.JobType.pyCfgParams = ['outputFile={0}'.format(output_name)]
        config.JobType.outputFiles = [output_name]
        config.Data.inputDataset = dataset
#        config.Data.inputDBS = 'phys03' # must be set to global (and this is the default)
        p = Process(target=submit, args=(config,))
        p.start()
        p.join()
