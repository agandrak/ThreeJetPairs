##################################################################
########   TO RUN THIS: python submit_multiple_jobs.py
########   DO NOT DO: crab submit submit_multiple_jobs.py
########
########   From https://github.com/alefisico/RUNA
##################################################################

from CRABClient.UserUtilities import config
from httplib import HTTPException
from multiprocessing import Process
from CRABAPI.RawCommand import crabCommand

def submit(config):
    try:
        crabCommand('submit', config = config)
    except HTTPException, hte:
        print 'Cannot execute command'
        print hte.headers

if __name__ == '__main__':

#
    Samples = [
        '/ScoutingPFHT/Run2016B-v2/RAW',
        '/ScoutingPFHT/Run2016C-v2/RAW',
		'/ScoutingPFHT/Run2016D-v2/RAW',
		'/ScoutingPFHT/Run2016E-v2/RAW',
		'/ScoutingPFHT/Run2016F-v1/RAW',
		'/ScoutingPFHT/Run2016G-v1/RAW',
		'/ScoutingPFHT/Run2016H-v1/RAW'
        ]

    config = config()

    version = 'v1'

    config.General.requestName = ''
    config.General.workArea = 'jobs'

    config.JobType.pluginName = 'Analysis'
    config.JobType.psetName = '../python/ScoutingNtuplizer_cfg.py'
    config.JobType.allowUndistributedCMSSW = True

    config.Data.inputDataset = ''
    config.Data.splitting = 'LumiBased'
    config.Data.unitsPerJob = 50 # to be determined, though 500 sounds like a good number for ScoutingPFHT
    config.Data.lumiMask = 'https://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions16/13TeV/Final/Cert_271036-284044_13TeV_PromptReco_Collisions16_JSON.txt'
    config.Data.publication = False
    config.Data.ignoreLocality = True

    config.Data.outLFNDirBase = "/store/user/agandrak/store/ntuples/PFScouting-Jun02_17"

    config.Site.storageSite = 'T3_US_Rutgers'


    for dataset in Samples:
        dataset_string = dataset.split('/ScoutingPFHT/')[1].split('/RAW')[0]
        dataset_name = dataset_string.split('-')[0]
        version = dataset_string.split('-')[1]
        name = 'Scouting_Ntuples_{0}_{1}'.format(dataset_name, version)
        config.General.requestName = name
        config.Data.outputDatasetTag = name
        output_name = 'scouting_ntuple.root'
        config.JobType.pyCfgParams = ['outputFile={0}'.format(output_name)]
        config.JobType.outputFiles = [output_name]
        config.Data.inputDataset = dataset
#        config.Data.inputDBS = 'phys03' # must be set to global (and this is the default)
        p = Process(target=submit, args=(config,))
        p.start()
        p.join()
