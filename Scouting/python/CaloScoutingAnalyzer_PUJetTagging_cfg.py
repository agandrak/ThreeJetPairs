import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("CaloScoutingAnalyzerPUJetTagging")

process.load("FWCore.MessageService.MessageLogger_cfi")
#CMSDIJET
process.load('PhysicsTools.PatAlgos.producersLayer1.patCandidates_cff')
process.load('Configuration.EventContent.EventContent_cff')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_AutoFromDBCurrent_cff')

## ----------------- test GT and outName -----------------
testGT = "80X_dataRun2_HLT_v12"
#testGT = "fake"
testOutName = "test_ntuple2.root"

## ----------------- Global Tag -----------------
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_condDBv2_cff')
process.GlobalTag.globaltag = "80X_dataRun2_HLT_v12" #in place of THISGLOBALTAG
options = VarParsing.VarParsing('analysis')
#TODO have the outputFile print the time it was produced as a parameter
#options.outputFile = 'scouting_ntuple.root'
#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/data/Run2016B/ScoutingPFHT/RAW/v2/000/273/150/00000/3AAF2452-C317-E611-9048-02163E014121.root'
#options.inputFiles == 'root://cms-xrd-global.cern.ch//store/data/Run2016D/ScoutingPFHT/RAW/v2/000/276/318/00000/FECCC11C-4F42-E611-BEEB-02163E0118C5.root'
#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/data/Run2016B/ScoutingPFHT/RAW/v2/000/273/411/00000/B2BA20D8-CF19-E611-87B8-02163E01344D.root'
options.inputFiles = 'file:/cms/duncan/1CDA7299-0C18-E611-81EB-02163E011EB0.root'
#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/data/Run2016B/ScoutingPFCommissioning/RAW/v2/000/273/554/00000/2216172D-E31C-E611-B1D4-02163E014142.root'
options.maxEvents = -1
options.register('reportEvery',
                 10000, # default value
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.int,
                 "Number of events to process before reporting progress.")
options.register('local',
                   False,
                   VarParsing.VarParsing.multiplicity.singleton,
                   VarParsing.VarParsing.varType.bool,
"Local running")


process.TFileService=cms.Service("TFileService",
                                 fileName=cms.string("scouting_ntuple.root"),
                                 closeFileFast = cms.untracked.bool(True)
)
process.options = cms.untracked.PSet(
        allowUnscheduled = cms.untracked.bool(True),
        wantSummary = cms.untracked.bool(False),
)
options.parseArguments()

process.TFileService.fileName = cms.string(options.outputFile)

if options.local == True:
    process.GlobalTag.globaltag = testGT
    process.TFileService.fileName = cms.string(testOutName)


process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
)
process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(
    options.reportEvery)

process.source = cms.Source(
    "PoolSource",
    fileNames = cms.untracked.vstring(options.inputFiles)
)

##------------------- l1 stage2 digis ------------------------------
process.load("EventFilter.L1TRawToDigi.gtStage2Digis_cfi")
process.gtStage2Digis.InputLabel = cms.InputTag( "hltFEDSelectorL1" )

##-------------------- User analyzer  --------------------------------
#import trigger conf
from ThreeJetAnalysis.Scouting.TriggerPath_cfi import getHLTConf, getL1Conf

#process.load('ThreeJetAnalysis.Scouting.analyzer_cfi')
process.analyzer = cms.EDAnalyzer(
    'CaloScoutingAnalyzer_PUJetTagging',
    jet_collection       = cms.InputTag('hltScoutingCaloPacker'),
#    candidate_collection = cms.InputTag('hltScoutingCaloPacker'),
    vertex_collection    = cms.InputTag('hltScoutingCaloPacker'),
    rho                  = cms.InputTag('hltScoutingCaloPacker:rho'),
    MET			 = cms.InputTag('hltScoutingCaloPacker:caloMetPt'),
    MET_phi		 = cms.InputTag('hltScoutingCaloPacker:caloMetPhi'),
    cut_nJets_min        = cms.int32(4),
#    output_file_name     = cms.string('scouting_ntuple.root'),
    # leptons
 #   electron_collection	= cms.InputTag('hltScoutingEgammaPacker'),
  #  muon_collection		= cms.InputTag('hltScoutingMuonPacker'),

    # JECs
    doJECs = cms.bool(True),
    
	####### 2016 PF Scouting #######
	L1corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L1FastJet_AK4CaloHLT.txt'),
	L2corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L2Relative_AK4CaloHLT.txt'),
	#L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L3Absolute_AK4CaloHLT.txt'),
	L2L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L2L3Residual_AK4CaloHLT.txt'),

    ## trigger ###################################
    triggerAlias = cms.vstring(getHLTConf(0)),
    triggerSelection = cms.vstring(getHLTConf(1)),
    triggerDuplicates = cms.vint32(getHLTConf(2)),

    triggerConfiguration = cms.PSet(
        hltResults            = cms.InputTag('TriggerResults','','HLT'),
        l1tResults            = cms.InputTag(''),
        daqPartitions         = cms.uint32(1),
#        l1tIgnoreMask         = cms.bool(False),
#        l1techIgnorePrescales = cms.bool(False),
		l1tIgnoreMaskAndPrescale = cms.bool(False),
        throw                 = cms.bool(False)
    ),

    #L1 trigger info
    doL1 = cms.bool(True),
    AlgInputTag = cms.InputTag("gtStage2Digis"),
    l1tAlgBlkInputTag = cms.InputTag("gtStage2Digis"),
    l1tExtBlkInputTag = cms.InputTag("gtStage2Digis"),

	l1Seeds = cms.vstring(getL1Conf())
)

process.analyzer.output_file_name = cms.string(options.outputFile)

process.p = cms.Path(process.analyzer)
