import FWCore.ParameterSet.Config as cms

theoryminituplizer = cms.EDAnalyzer(
    'TheoryMiniTuplizer',
    jet_collection      = cms.InputTag('hltScoutingPFPacker'),
    rho					= cms.InputTag('hltScoutingPFPacker:rho'),
    muon_collection     = cms.InputTag('hltScoutingMuonPacker'),
    electron_collection = cms.InputTag('hltScoutingEgammaPacker'),
    photon_collection   = cms.InputTag('hltScoutingEgammaPacker'),
    MET_pt              = cms.InputTag('hltScoutingPFPacker:pfMetPt'),
    MET_phi             = cms.InputTag('hltScoutingPFPacker:pfMetPhi'),
    output_file_name    = cms.string('theory_minituple.root'),
    L1corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L1FastJet_AK4PFHLT.txt'),
	L2corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L2Relative_AK4PFHLT.txt'),
	L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L3Absolute_AK4PFHLT.txt'),
	L2L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L2L3Residual_AK4PFHLT.txt')
)
