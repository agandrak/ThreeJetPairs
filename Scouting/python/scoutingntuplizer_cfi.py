import FWCore.ParameterSet.Config as cms
from ThreeJetAnalysis.Scouting.TriggerPath_cfi import getHLTConf, getL1Conf

scoutingntuplizer = cms.EDAnalyzer(
    'ScoutingNtuplizer',
    jet_collection       = cms.InputTag('hltScoutingPFPacker'),
    candidate_collection = cms.InputTag('hltScoutingPFPacker'),
    vertex_collection    = cms.InputTag('hltScoutingPFPacker'),
    rho                  = cms.InputTag('hltScoutingPFPacker:rho'),
    MET			 = cms.InputTag('hltScoutingPFPacker:pfMetPt'),
    MET_phi		 = cms.InputTag('hltScoutingPFPacker:pfMetPhi'),
    cut_nJets_min        = cms.int32(4),
    output_file_name     = cms.string('scouting_ntuple.root'),

	# Keep in case you ever need to use Calo Scouting again
#	jet_collection       = cms.InputTag('hltScoutingCaloPacker'),
#	candidate_collection = cms.InputTag('hltScoutingCaloPacker'),
#	vertex_collection    = cms.InputTag('hltScoutingCaloPacker'),
#	rho                  = cms.InputTag('hltScoutingCaloPacker:rho'),
#	MET			 = cms.InputTag('hltScoutingCaloPacker:caloMetPt'),
#	cut_nJets_min        = cms.int32(4),
#	output_file_name     = cms.string('scouting_ntuple.root'),

    # leptons
    electron_collection	= cms.InputTag('hltScoutingEgammaPacker'),
    muon_collection		= cms.InputTag('hltScoutingMuonPacker'),

    # JECs
    doJECs = cms.bool(True),
    
	####### 2016 PF Scouting #######
	L1corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L1FastJet_AK4PFHLT.txt'),
	L2corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L2Relative_AK4PFHLT.txt'),
	L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L3Absolute_AK4PFHLT.txt'),
	L2L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/80X_dataRun2_HLT_v12/80X_dataRun2_HLT_v12_L2L3Residual_AK4PFHLT.txt'),
    
    
	####### 2015 PF Scouting #######    
#	L1corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_MC/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_L1FastJet_AK4PFchs.txt'),
#	L2corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_MC/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_L2Relative_AK4PFchs.txt'),
#	L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_MC/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_L3Absolute_AK4PFchs.txt'),
#	L2L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/Summer15_25nsV7_DATA/Summer15_25nsV7_DATA_L2L3Residual_AK4PFchs.txt')
    
    
	####### 2015 Calo Scouting #######    
#    L1corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_MC/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_L1FastJet_AK4CaloHLT.txt'),
#    L2corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_MC/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_L2Relative_AK4CaloHLT.txt'),
#    L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_MC/74X_HLT_mcRun2_asymptotic_fromSpring15DR_v0_L3Absolute_AK4CaloHLT.txt'),
#    L2L3corrAK4_DATA = cms.FileInPath('ThreeJetAnalysis/Scouting/data/Summer15_25nsV7_DATA/Summer15_25nsV7_DATA_L2L3Residual_AK4PF.txt')

    ## trigger ###################################
    triggerAlias = cms.vstring(getHLTConf(0)),
    triggerSelection = cms.vstring(getHLTConf(1)),
    triggerDuplicates = cms.vint32(getHLTConf(2)),

    triggerConfiguration = cms.PSet(
        hltResults            = cms.InputTag('TriggerResults','','HLT'),
        l1tResults            = cms.InputTag(''),
        daqPartitions         = cms.uint32(1),
        l1tIgnoreMask         = cms.bool(False),
        l1techIgnorePrescales = cms.bool(False),
        throw                 = cms.bool(False)
    ),

    #L1 trigger info
    doL1 = cms.bool(True),
    AlgInputTag = cms.InputTag("gtStage2Digis"),
    l1tAlgBlkInputTag = cms.InputTag("gtStage2Digis"),
    l1tExtBlkInputTag = cms.InputTag("gtStage2Digis"),

	l1Seeds = cms.vstring(getL1Conf())
    )

