import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

process = cms.Process("TheoryMiniTuplizer")

process.load("FWCore.MessageService.MessageLogger_cfi")

options = VarParsing.VarParsing('analysis')
options.outputFile = 'theory_minituple.root'
#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/data/Run2016B/ScoutingPFHT/RAW/v2/000/273/150/00000/3AAF2452-C317-E611-9048-02163E014121.root'
#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/data/Run2015D/ScoutingPFMuons/RAW/v1/000/259/636/00000/B4F4CB50-2678-E511-8111-02163E011EB2.root' #FAILED
#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/data/Run2015D/ScoutingPFHT/RAW/v1/000/257/968/00000/26CEDB20-FC67-E511-A769-02163E0139A8.root'
#options.inputFiles = 'root://cms-xrd-global.cern.ch//store/data/Run2015D/ScoutingPFHT/RAW/v1/000/260/423/00000/98FE8B74-9280-E511-A5A8-02163E013889.root'
options.inputFiles = 'root://cms-xrd-global.cern.ch//store/data/Run2016D/ScoutingPFMuons/RAW/v2/000/276/315/00000/6A508A0C-0742-E611-801E-02163E012179.root'
options.maxEvents = -1
options.register('reportEvery',
                 1000000, # default value
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.int,
                 "Number of events to process before reporting progress.")
options.parseArguments()

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
)
process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(
    options.reportEvery)

process.source = cms.Source(
    "PoolSource",
    fileNames = cms.untracked.vstring(options.inputFiles)
)

process.load('ThreeJetAnalysis.Scouting.theoryminituplizer_cfi')

process.theoryminituplizer.output_file_name = cms.string(options.outputFile)

process.p = cms.Path(process.theoryminituplizer)
