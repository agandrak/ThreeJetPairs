// -*- C++ -*-
//
// Package:    ThreeJetAnalysis/Scouting
// Class:      ScoutingNtuplizer
//
/**\class ScoutingNtuplizer ScoutingNtuplizer.h ThreeJetAnalysis/Scouting/interface/ScoutingNtuplizer.h

 Description: Code to monitor scouting streams.

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  David Sheffield
//         Created:  Wed, 28 Oct 2015
//
//


// System include files
#include <memory>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

// CMSSW include files
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "DataFormats/Scouting/interface/ScoutingPFJet.h"
#include "DataFormats/Scouting/interface/ScoutingCaloJet.h"
#include "DataFormats/Scouting/interface/ScoutingParticle.h"
#include "DataFormats/Scouting/interface/ScoutingVertex.h"
#include "DataFormats/Scouting/interface/ScoutingMuon.h"
#include "DataFormats/Scouting/interface/ScoutingElectron.h"
#include "DataFormats/Scouting/interface/ScoutingPhoton.h"
#include "HLTrigger/HLTcore/interface/TriggerExpressionData.h"
#include "HLTrigger/HLTcore/interface/TriggerExpressionEvaluator.h"
#include "HLTrigger/HLTcore/interface/TriggerExpressionParser.h"
#include "L1Trigger/L1TGlobal/interface/L1TGlobalUtil.h"


// CMSSW include files for JECs

#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"
#include "FWCore/ParameterSet/interface/FileInPath.h"

// Included just in case!!!

#include "HLTrigger/HLTcore/interface/TriggerExpressionData.h"
#include "HLTrigger/HLTcore/interface/TriggerExpressionEvaluator.h"
#include "HLTrigger/HLTcore/interface/TriggerExpressionParser.h"

// Root include files
#include "TLorentzVector.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"

// User include files
#include "ThreeJetAnalysis/Utilities/interface/TH1DInitializer.h"
#include "ThreeJetAnalysis/Utilities/interface/TH2DInitializer.h"

//
// class declaration
//

class ScoutingNtuplizer : public edm::EDAnalyzer {
public:
    explicit ScoutingNtuplizer(const edm::ParameterSet&);
    ~ScoutingNtuplizer();

    static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);


private:
    virtual void beginJob() override;
    virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
    virtual void endJob() override;
    virtual void ResetVariables();
	virtual void SortPts();
    virtual int GetCollections(const edm::Event&);

    // ----------member data ---------------------------
    edm::EDGetTokenT<ScoutingPFJetCollection> token_jets;
    edm::EDGetTokenT<ScoutingParticleCollection> token_candidates;
    edm::EDGetTokenT<ScoutingVertexCollection> token_vertices;
    edm::EDGetTokenT<double> token_rho;
    edm::EDGetTokenT<double> token_MET;
    edm::EDGetTokenT<double> token_MET_phi;

    edm::Handle<ScoutingPFJetCollection> jets;
	edm::Handle<ScoutingParticleCollection> candidates;
    edm::Handle<ScoutingVertexCollection> vertices;
    edm::Handle<double> handle_rho;
    edm::Handle<double> handle_MET;
    edm::Handle<double> handle_MET_phi;

    int cut_nJets_min;

//    std::string file_name;
	edm::Service<TFileService> fs_;
    TFile *file;
    TTree *tree;

    float Ht;
    float Ht_20;
    float Ht_raw;
//    float HtJEC;
    int event_num_;

    int jet_num;
    int jet_num_20;
    int jet_num_raw;

    std::vector<float> jet_pt;
    std::vector<float> jet_eta;
    std::vector<float> jet_phi;
    std::vector<float> jet_m;
    std::vector<float> jet_energy_correction;
    std::vector<float> jet_csv;
	std::vector<float> jet_ID;

	std::vector<float> jet_energy;
	std::vector<float> jet_NHF;
	std::vector<float> jet_CHF;
	std::vector<float> jet_NEMF;
	std::vector<float> jet_CEMF;
//std::vector<float> jet_MUF;
/*
	std::vector<float> jet_HF_HF;
	std::vector<float> jet_HF_EMF;
	std::vector<float> jet_HOF;
*/
	std::vector<int> jet_CM;
	std::vector<int> jet_NM;
	std::vector<int> jet_CHM;
	std::vector<int> jet_NHM;
//	std::vector<int> jet_PM;

	// raw

    std::vector<float> jet_pt_raw;
    std::vector<float> jet_eta_raw;
    std::vector<float> jet_phi_raw;
    std::vector<float> jet_m_raw;
    std::vector<float> jet_energy_correction_raw;
    std::vector<float> jet_csv_raw;
	std::vector<float> jet_ID_raw;

	std::vector<float> jet_energy_raw;
	std::vector<float> jet_NHF_raw;
	std::vector<float> jet_CHF_raw;
	std::vector<float> jet_NEMF_raw;
	std::vector<float> jet_CEMF_raw;
//	std::vector<float> jet_MUF_raw;
/*
	std::vector<float> jet_HF_HF_raw;
	std::vector<float> jet_HF_EMF_raw;
	std::vector<float> jet_HOF_raw;
*/
	std::vector<int> jet_CM_raw;
	std::vector<int> jet_NM_raw;
	std::vector<int> jet_CHM_raw;
	std::vector<int> jet_NHM_raw;
//	std::vector<int> jet_PM_raw;


    int vertex_num;

    float rho;
    float MET;
    float MET_phi;

	// lepton variables
	edm::EDGetTokenT<ScoutingElectronCollection> token_electrons;
	edm::EDGetTokenT<ScoutingMuonCollection> token_muons;

	edm::Handle<ScoutingElectronCollection> electrons;
	edm::Handle<ScoutingMuonCollection> muons;

	bool doJECs;
	
	int electron_num;
	std::vector<float> electron_pt;
	std::vector<float> electron_eta;
	std::vector<float> electron_phi;
	int muon_num;
	std::vector<float> muon_pt;
	std::vector<float> muon_eta;
	std::vector<float> muon_phi;

    //---- TRIGGER -------------------------
    triggerExpression::Data triggerCache_;
    std::vector<triggerExpression::Evaluator*> vtriggerSelector_;
    std::vector<std::string> vtriggerAlias_, vtriggerSelection_;
    std::vector<int> vtriggerDuplicates_;
    TH1F *triggerPassHisto_, *triggerNamesHisto_;
    TH1F *l1PassHisto_, *l1NamesHisto_;
    //---- L1 ----
    bool doL1_;
    edm::EDGetToken algToken_;
    l1t::L1TGlobalUtil *l1GtUtils_;
    std::vector<std::string> l1Seeds_;
	std::vector<bool> *l1Result_;

 	std::vector<bool> *triggerResult_;

    // for JECs
    edm::FileInPath L1corrAK4_DATA_, L2corrAK4_DATA_, L3corrAK4_DATA_,
        L2L3corrAK4_DATA_;
    JetCorrectorParameters *L1ParAK4_DATA;
    JetCorrectorParameters *L2ParAK4_DATA;
    JetCorrectorParameters *L3ParAK4_DATA;
    JetCorrectorParameters *L2L3ResAK4_DATA;
    FactorizedJetCorrector *JetCorrectorAK4_DATA;
	
	// Amit asked to see
	TH1F *eventJetIdPassHisto_, *jetIdPassHisto_;


    int run;
    int lumi;
    int event;
};

//bool SortWbPairs(const std::pair<float, float>, const std::pair<float, float>);
