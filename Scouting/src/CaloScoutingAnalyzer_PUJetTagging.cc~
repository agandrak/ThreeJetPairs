// -*- C++ -*-
//
// Package:    ThreeJetAnalysis/Scouting
// Class:      CaloScoutingAnalyzer_PUJetTagging
//
/**\class CaloScoutingAnalyzer_PUJetTagging CaloScoutingAnalyzer_PUJetTagging.cc ThreeJetAnalysis/Scouting/src/CaloScoutingAnalyzer_PUJetTagging.cc

 Description: Code to monitor scouting streams.

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  David Sheffield
//         Created:  Wed, 28 Oct 2015
//
//

#include "ThreeJetAnalysis/Scouting/interface/CaloScoutingAnalyzer_PUJetTagging.h"

using namespace std;
using namespace edm;
using namespace l1t;

template <typename T, typename Compare>
std::vector<std::size_t> sort_pts_algo(
    const std::vector<T>& vec, 
	Compare& compare)
{
    std::vector<std::size_t> p(vec.size());
    std::iota(p.begin(), p.end(), 0);
    std::sort(p.begin(), p.end(),
        [&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
    return p;
}

bool CaloScoutingAnalyzer_PUJetTagging::greaterThan( double a, double b) {
	return a > b;
}

template <typename T>
void apply_permutation(
    std::vector<T>& vec,
    const std::vector<std::size_t>& p)
{
    std::vector<bool> done(vec.size());
    for (std::size_t i = 0; i < vec.size(); ++i)
    {
        if (done[i])
        {
            continue;
        }
        done[i] = true;
        std::size_t prev_j = i;
        std::size_t j = p[i];
        while (i != j)
        {
            std::swap(vec[prev_j], vec[j]);
            done[j] = true;
            prev_j = j;
            j = p[j];
        }
    }
}

//
// constructors and destructor
//
CaloScoutingAnalyzer_PUJetTagging::CaloScoutingAnalyzer_PUJetTagging(const edm::ParameterSet& iConfig):
    token_jets(consumes<ScoutingCaloJetCollection>(
                   iConfig.getParameter<InputTag>("jet_collection"))),
    token_vertices(consumes<ScoutingVertexCollection>(
                       iConfig.getParameter<InputTag>("vertex_collection"))),
    token_rho(consumes<double>(
                  iConfig.getParameter<InputTag>("rho"))),
    token_MET(consumes<double>(
                  iConfig.getParameter<InputTag>("MET"))),
    token_MET_phi(consumes<double>(
                  iConfig.getParameter<InputTag>("MET_phi"))),
    cut_nJets_min(iConfig.getParameter<int>("cut_nJets_min")),
	doJECs((iConfig.getParameter<bool>("doJECs"))),
    triggerCache_(triggerExpression::Data(
                      iConfig.getParameterSet("triggerConfiguration"),
                      consumesCollector())),
    vtriggerAlias_(iConfig.getParameter<vector<string>>("triggerAlias")),
    vtriggerSelection_(iConfig.getParameter<vector<string>>("triggerSelection")),
    vtriggerDuplicates_(iConfig.getParameter<vector<int>>("triggerDuplicates")),
	doL1_(iConfig.getParameter<bool>("doL1"))
{
    //now do what ever initialization is needed
    if (vtriggerAlias_.size() != vtriggerSelection_.size()) {
        cout << "ERROR: The number of trigger aliases does not match the number of trigger names!!!"
             << endl;
        return;
    }
    if (vtriggerDuplicates_.size() != vtriggerSelection_.size()) {
        cout << "ERROR: The size of trigger duplicates vector does not match the number of trigger names."
             << endl;
        return;
    }
    for (unsigned i=0; i<vtriggerSelection_.size(); ++i) {
        vtriggerSelector_.push_back(triggerExpression::parse(
                                        vtriggerSelection_[i]));
	}


    L1corrAK4_DATA_ = iConfig.getParameter<FileInPath>("L1corrAK4_DATA");
    L2corrAK4_DATA_ = iConfig.getParameter<FileInPath>("L2corrAK4_DATA");
   // L3corrAK4_DATA_ = iConfig.getParameter<FileInPath>("L3corrAK4_DATA");
    L2L3corrAK4_DATA_ = iConfig.getParameter<FileInPath>("L2L3corrAK4_DATA");

    L1ParAK4_DATA = new JetCorrectorParameters(L1corrAK4_DATA_.fullPath());
    L2ParAK4_DATA = new JetCorrectorParameters(L2corrAK4_DATA_.fullPath());
   // L3ParAK4_DATA = new JetCorrectorParameters(L3corrAK4_DATA_.fullPath());
    L2L3ResAK4_DATA = new JetCorrectorParameters(L2L3corrAK4_DATA_.fullPath());

    vector<JetCorrectorParameters> vParAK4_DATA;
    vParAK4_DATA.push_back(*L1ParAK4_DATA);
    vParAK4_DATA.push_back(*L2ParAK4_DATA);
   // vParAK4_DATA.push_back(*L3ParAK4_DATA);
    vParAK4_DATA.push_back(*L2L3ResAK4_DATA);

    JetCorrectorAK4_DATA = new FactorizedJetCorrector(vParAK4_DATA);

	
}


CaloScoutingAnalyzer_PUJetTagging::~CaloScoutingAnalyzer_PUJetTagging()
{
}


//
// member functions
//

// ------------ method called for each event  ------------
void
CaloScoutingAnalyzer_PUJetTagging::analyze(const edm::Event& iEvent,
                         const edm::EventSetup& iSetup)
{

    ResetVariables();


    run = iEvent.id().run();
    lumi = iEvent.id().luminosityBlock();
    event = iEvent.id().event();
//	cout << event << endl;

    int getCollectionsResult = GetCollections(iEvent);
    if (getCollectionsResult)
		return;
		
	bool isEvent = true;
	bool hasOneTaggedJet = false;
	bool hasSixTaggedJets = false;
	

    for (ScoutingCaloJetCollection::const_iterator ijet=jets->begin(); ijet!=jets->end(); ++ijet) {

		double jet_e = ijet->emEnergyInEB() + ijet->emEnergyInEE()
                          + ijet->emEnergyInHF() + ijet->hadEnergyInHB()
+ ijet->hadEnergyInHE() + ijet->hadEnergyInHF();
		double HF = (ijet->hadEnergyInHB()+ijet->hadEnergyInHE()+ijet->hadEnergyInHF())/jet_e;
		double EMF = (ijet->emEnergyInEB()+ijet->emEnergyInEE()+ijet->emEnergyInHF())/jet_e;


		bool looseJetID = false;
		bool tightJetID = false;
		if (ijet->btagDiscriminator()>=0) tightJetID = true;

		if (HF < .95 && EMF < .95) looseJetID = true;

		int jetID = 0;
		if (tightJetID && looseJetID) jetID = 2;
		else if (tightJetID) jetID = 1;
		else if (looseJetID) jetID = 0;
		else jetID = -1;
	
		if (jetID >= 1) {
			jet_num++; // will be cutting on this for three jet analysis
		}
    }
    
    if (jet_num > 0) hasOneTaggedJet = true;
    if (jet_num >= 6) hasSixTaggedJets = true;
    
    if (isEvent) eventTag->Fill(0);
    if (hasOneTaggedJet) eventTag->Fill(1);
    if (hasSixTaggedJets) eventTag->Fill(2);

    return;
}


// ------- method called once each job just before starting event loop  -------
void CaloScoutingAnalyzer_PUJetTagging::beginJob()
{
	cout << "CaloScoutingAnalyzer_PUJetTagging::beginJob()" << endl;
	
	eventTag = fs_->make<TH1D>("eventTag", "eventTag", 3, -.5, 2.5);
	

}

// ------- method called once each job just after ending the event loop  -------
void CaloScoutingAnalyzer_PUJetTagging::endJob()
{

	cout << "CaloScoutingAnalyzer_PUJetTagging::endJob()" << endl;
    for(unsigned i=0; i<vtriggerSelector_.size(); ++i) {
        delete vtriggerSelector_[i];
	}
}


// -- method fills 'descriptions' with the allowed parameters for the module  --
void CaloScoutingAnalyzer_PUJetTagging::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}


void CaloScoutingAnalyzer_PUJetTagging::ResetVariables()
{


    Ht = 0.0;
	Ht_20 = 0.0;
	Ht_raw = 0.0;


    // jets w/ eta <= 2.4
    jet_num = 0;
	jet_num_20 = 0;
	jet_num_raw = 0;

    jet_pt.clear();
    jet_eta.clear();
	jet_phi.clear();
	jet_m.clear();
	jet_csv.clear();
	jet_energy_correction.clear();
	jet_ID.clear();

	jet_energy.clear();
	jet_HF.clear();
	jet_EMF.clear();


	// crap jets	
    jet_pt_raw.clear();
    jet_eta_raw.clear();
	jet_phi_raw.clear();
	jet_m_raw.clear();
	jet_csv_raw.clear();
	jet_energy_correction_raw.clear();
	jet_ID_raw.clear();

	jet_energy_raw.clear();
	jet_HF_raw.clear();
	jet_EMF_raw.clear();


	// jet w/ 2.4 < eta <= 3.0

    // electrons
    electron_num = 0;
    electron_pt.clear();
    electron_eta.clear();
    electron_phi.clear();

    // muons
    muon_num = 0;
    muon_pt.clear();
    muon_eta.clear();
    muon_phi.clear();

    vertex_num = 0;

    rho = 0.0;
    MET = 0.0;
    MET_phi = 0.0;

    run = 0;
    lumi = 0;
    event = 0;

    return;
}

void CaloScoutingAnalyzer_PUJetTagging::SortPts() {
	// look into making a class that does this

	auto indices = sort_pts_algo(jet_pt, greaterThan);

    apply_permutation(jet_pt, indices);
    apply_permutation(jet_eta, indices);
	apply_permutation(jet_phi, indices);
	apply_permutation(jet_m, indices);
	apply_permutation(jet_csv, indices);
	apply_permutation(jet_energy_correction, indices);
	apply_permutation(jet_ID, indices);

	apply_permutation(jet_HF, indices);
	apply_permutation(jet_EMF, indices);

	indices = sort_pts_algo(jet_pt_raw, greaterThan);

	// crap jets	
    apply_permutation(jet_pt_raw, indices);
    apply_permutation(jet_eta_raw, indices);
	apply_permutation(jet_phi_raw, indices);
	apply_permutation(jet_m_raw, indices);
	apply_permutation(jet_csv_raw, indices);
	apply_permutation(jet_energy_correction_raw, indices);
	apply_permutation(jet_ID_raw, indices);

	apply_permutation(jet_HF_raw, indices);
	apply_permutation(jet_EMF_raw, indices);
}

int CaloScoutingAnalyzer_PUJetTagging::GetCollections(const edm::Event& iEvent)
{
    // Get collections from ntuple
    // Returns nonzero if there is a problem getting a collection

    // Get jets
    iEvent.getByToken(token_jets, jets);
    if (!jets.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find ScoutingCaloJetCollection." << endl;
	return 1;
    }

    // Get vertices
    iEvent.getByToken(token_vertices, vertices);
    if (!vertices.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find ScoutingVertexCollection." << endl;
	return 1;
    }

    // Get rho
    iEvent.getByToken(token_rho, handle_rho);
    if (!handle_rho.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find rho." << endl;
	return 1;
    }

    // Get MET
    iEvent.getByToken(token_MET, handle_MET);
    if (!handle_MET.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find MET." << endl;
	return 1;
    }

    iEvent.getByToken(token_MET_phi, handle_MET_phi);
    if (!handle_MET_phi.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find MET_phi." << endl;
	return 1;
    }

    return 0;
}

//define this as a plug-in
DEFINE_FWK_MODULE(CaloScoutingAnalyzerPUJetTagging);
