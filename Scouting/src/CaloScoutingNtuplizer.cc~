// -*- C++ -*-
//
// Package:    ThreeJetAnalysis/Scouting
// Class:      CaloScoutingNtuplizer
//
/**\class CaloScoutingNtuplizer CaloScoutingNtuplizer.cc ThreeJetAnalysis/Scouting/src/CaloScoutingNtuplizer.cc

 Description: Code to monitor scouting streams.

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  David Sheffield
//         Created:  Wed, 28 Oct 2015
//
//

#include "ThreeJetAnalysis/Scouting/interface/CaloScoutingNtuplizer.h"

using namespace std;
using namespace edm;
using namespace l1t;

template <typename T, typename Compare>
std::vector<std::size_t> sort_pts_algo(
    const std::vector<T>& vec, 
	Compare& compare)
{
    std::vector<std::size_t> p(vec.size());
    std::iota(p.begin(), p.end(), 0);
    std::sort(p.begin(), p.end(),
        [&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
    return p;
}

bool CaloScoutingNtuplizer::greaterThan( double a, double b) {
	return a > b;
}

template <typename T>
void apply_permutation(
    std::vector<T>& vec,
    const std::vector<std::size_t>& p)
{
    std::vector<bool> done(vec.size());
    for (std::size_t i = 0; i < vec.size(); ++i)
    {
        if (done[i])
        {
            continue;
        }
        done[i] = true;
        std::size_t prev_j = i;
        std::size_t j = p[i];
        while (i != j)
        {
            std::swap(vec[prev_j], vec[j]);
            done[j] = true;
            prev_j = j;
            j = p[j];
        }
    }
}

//
// constructors and destructor
//
CaloScoutingNtuplizer::CaloScoutingNtuplizer(const edm::ParameterSet& iConfig):
    token_jets(consumes<ScoutingCaloJetCollection>(
                   iConfig.getParameter<InputTag>("jet_collection"))),
/*    token_candidates(consumes<ScoutingParticleCollection>(
                         iConfig.getParameter<InputTag>(
                             "candidate_collection"))),*/
    token_vertices(consumes<ScoutingVertexCollection>(
                       iConfig.getParameter<InputTag>("vertex_collection"))),
    token_rho(consumes<double>(
                  iConfig.getParameter<InputTag>("rho"))),
    token_MET(consumes<double>(
                  iConfig.getParameter<InputTag>("MET"))),
    token_MET_phi(consumes<double>(
                  iConfig.getParameter<InputTag>("MET_phi"))),
    cut_nJets_min(iConfig.getParameter<int>("cut_nJets_min")),
//    file_name(iConfig.getParameter<string>("output_file_name")),

	//token_electrons(consumes<ScoutingElectronCollection>(
                  // iConfig.getParameter<InputTag>("electron_collection"))),
	//token_muons(consumes<ScoutingMuonCollection>(
          //         iConfig.getParameter<InputTag>("muon_collection"))),
	doJECs((iConfig.getParameter<bool>("doJECs"))),
    triggerCache_(triggerExpression::Data(
                      iConfig.getParameterSet("triggerConfiguration"),
                      consumesCollector())),
    vtriggerAlias_(iConfig.getParameter<vector<string>>("triggerAlias")),
    vtriggerSelection_(iConfig.getParameter<vector<string>>("triggerSelection")),
    vtriggerDuplicates_(iConfig.getParameter<vector<int>>("triggerDuplicates")),
	doL1_(iConfig.getParameter<bool>("doL1"))
{
    //now do what ever initialization is needed
    if (vtriggerAlias_.size() != vtriggerSelection_.size()) {
        cout << "ERROR: The number of trigger aliases does not match the number of trigger names!!!"
             << endl;
        return;
    }
    if (vtriggerDuplicates_.size() != vtriggerSelection_.size()) {
        cout << "ERROR: The size of trigger duplicates vector does not match the number of trigger names."
             << endl;
        return;
    }
    for (unsigned i=0; i<vtriggerSelection_.size(); ++i) {
        vtriggerSelector_.push_back(triggerExpression::parse(
                                        vtriggerSelection_[i]));
	}

//    file = new TFile(file_name.c_str(), "RECREATE");

//    tree = new TTree("events", "Tree for scouting data");

    L1corrAK4_DATA_ = iConfig.getParameter<FileInPath>("L1corrAK4_DATA");
    L2corrAK4_DATA_ = iConfig.getParameter<FileInPath>("L2corrAK4_DATA");
   // L3corrAK4_DATA_ = iConfig.getParameter<FileInPath>("L3corrAK4_DATA");
    L2L3corrAK4_DATA_ = iConfig.getParameter<FileInPath>("L2L3corrAK4_DATA");

    L1ParAK4_DATA = new JetCorrectorParameters(L1corrAK4_DATA_.fullPath());
    L2ParAK4_DATA = new JetCorrectorParameters(L2corrAK4_DATA_.fullPath());
   // L3ParAK4_DATA = new JetCorrectorParameters(L3corrAK4_DATA_.fullPath());
    L2L3ResAK4_DATA = new JetCorrectorParameters(L2L3corrAK4_DATA_.fullPath());

    vector<JetCorrectorParameters> vParAK4_DATA;
    vParAK4_DATA.push_back(*L1ParAK4_DATA);
    vParAK4_DATA.push_back(*L2ParAK4_DATA);
   // vParAK4_DATA.push_back(*L3ParAK4_DATA);
    vParAK4_DATA.push_back(*L2L3ResAK4_DATA);

    JetCorrectorAK4_DATA = new FactorizedJetCorrector(vParAK4_DATA);

    if (doL1_) {
        algToken_ = consumes<BXVector<GlobalAlgBlk>>(iConfig.getParameter<InputTag>("AlgInputTag"));
        l1Seeds_ = iConfig.getParameter<std::vector<std::string> >("l1Seeds");
        l1GtUtils_ = new L1TGlobalUtil(iConfig,consumesCollector());
    }
    else {
        l1Seeds_ = std::vector<std::string>();
        l1GtUtils_ = 0;
	}
	
}


CaloScoutingNtuplizer::~CaloScoutingNtuplizer()
{

    // do anything here that needs to be done at desctruction time
    // (e.g. close files, deallocate resources etc.)
//    file->cd();
//    tree->Write();
//    file->Close();
// DONT DO ANYTHING

}


//
// member functions
//

// ------------ method called for each event  ------------
void
CaloScoutingNtuplizer::analyze(const edm::Event& iEvent,
                         const edm::EventSetup& iSetup)
{

    ResetVariables();


    run = iEvent.id().run();
    lumi = iEvent.id().luminosityBlock();
    event = iEvent.id().event();
//	cout << event << endl;

    int getCollectionsResult = GetCollections(iEvent);
    if (getCollectionsResult)
		return;
	
	std::vector<int> analysis_jetID_list;
	int numberGoodJets = 0; // just in place to keep track of observed issue with saving of jetID information

    for (ScoutingCaloJetCollection::const_iterator ijet=jets->begin(); ijet!=jets->end(); ++ijet) {

//		if (ijet->chargedHadronEnergy() == 0) continue;
//		TLorentzVector tmp;
//		tmp.SetPtEtaPhiM(ijet->pt(), ijet->eta(), ijet->phi(), ijet->m());

//		double energy_RAW = tmp.E();
//		double jet_e = ijet->photonEnergy() + ijet->chargedHadronEnergy() + ijet->neutralHadronEnergy() + ijet->electronEnergy() + ijet->muonEnergy();
		double jet_e = ijet->emEnergyInEB() + ijet->emEnergyInEE()
                          + ijet->emEnergyInHF() + ijet->hadEnergyInHB()
+ ijet->hadEnergyInHE() + ijet->hadEnergyInHF();
		double HF = (ijet->hadEnergyInHB()+ijet->hadEnergyInHE()+ijet->hadEnergyInHF())/jet_e;
		double EMF = (ijet->emEnergyInEB()+ijet->emEnergyInEE()+ijet->emEnergyInHF())/jet_e;
//		double CHF = ijet->chargedHadronEnergy()/jet_e;
//		double MUF = ijet->muonEnergy()/jet_e;
//		double CEMF = ijet->electronEnergy()/jet_e;

//        double HF_HF = ijet->HFHadronEnergy()/jet_e;
//        double HF_EMF = ijet->HFEMEnergy()/jet_e;
//		double HOF = ijet->HOEnergy()/jet_e;

//		int CM = ijet->chargedHadronMultiplicity() + ijet->electronMultiplicity() + ijet->muonMultiplicity();
//		int NM = ijet->neutralHadronMultiplicity() + ijet->photonMultiplicity();

//		int CHM = ijet->chargedHadronMultiplicity();
//		int NHM = ijet->neutralHadronMultiplicity();
//		int PM = ijet->photonMultiplicity();

//		int NC = CM + NM;

		bool looseJetID = false;
		bool tightJetID = false;
		if (ijet->btagDiscriminator()>=0) tightJetID = true;
		// not worrying about lepton veto jetid
		if (HF < .95 && EMF < .95) looseJetID = true;

		// just going to go ahead naive to the jet energyies not matching
//		if (!EnergiesEqual(energy_RAW, jet_energy && ijet->electronEnergy()==0)) {
//			CEMF = abs(energy_RAW - jet_energy)/energy_RAW);
//			jet_energy = energy_RAW;
//		} // own correction to problem observed with Scouting data
/*
		if (fabs(ijet->eta()) <= 2.7) {
			looseJetID = (HF<0.99 && EMF<0.99 && NC>1) && ((abs(ijet->eta())<=2.4 && CHF>0 && CM>0 && CEMF<0.99) || fabs(ijet->eta())>2.4);
			
			tightJetID = (HF<0.90 && EMF<0.90 && NC>1) && ((abs(ijet->eta())<=2.4 && CHF>0 && CM>0 && CEMF<0.99) || fabs(ijet->eta())>2.4);
		}
		else if (fabs(ijet->eta()) > 2.7 && fabs(ijet->eta()) <= 3.0) {
			looseJetID = (HF<0.98 && EMF>0.01 && NM>2);

			tightJetID = (HF<0.98 && EMF>0.01 && NM>2);
		}*/
//		else continue;

	    double correctionJEC = 1.0; // now apply corrections
		if (doJECs) {
			JetCorrectorAK4_DATA->setJetEta(ijet->eta());
			JetCorrectorAK4_DATA->setJetPt(ijet->pt());
			JetCorrectorAK4_DATA->setJetA(ijet->jetArea());
			JetCorrectorAK4_DATA->setRho(rho);
			correctionJEC = JetCorrectorAK4_DATA->getCorrection();
		}

/*		if ( !looseJetID && !tightJetID ) {
			if (ijet->pt()*correctionJEC >= 30 && fabs(ijet->eta()) <= 2.4) 
				analysis_jetID_list.push_back(0);
			continue; //Always throw away crap jets
		}*/
		numberGoodJets++; // checking that stuff goes through

		int jetID = 0;
		if (tightJetID && looseJetID) jetID = 2;
		else if (tightJetID) jetID = 1;
		else if (looseJetID) jetID = 0;
		else jetID = -1;

//		if ( ijet->pt()*correctionJEC < 20 ) continue; // simply don't care about anything below 20 GeV (it's crap)
	
		if (fabs(ijet->eta()) <= 2.4 && jetID == 2) {
			Ht_20 += ijet->pt()*correctionJEC;
			jet_num_20++; // cut on this at ntuple level to allow abhijith to use this for ttbar studies
			if (ijet->pt()*correctionJEC >= 30.0) {
				Ht += ijet->pt()*correctionJEC;
				jet_num++; // will be cutting on this for three jet analysis
				analysis_jetID_list.push_back(jetID);
//				jetIdPassHisto_->Fill(jetID);
			}
			jet_pt.push_back(ijet->pt()*correctionJEC);
			jet_energy_correction.push_back(correctionJEC);
			jet_eta.push_back(ijet->eta());
			jet_phi.push_back(ijet->phi());
			jet_m.push_back(ijet->m()*correctionJEC);
			jet_csv.push_back(ijet->btagDiscriminator());
			jet_ID.push_back(jetID);
	
			jet_energy.push_back(jet_e*correctionJEC);
			// do not correct subsequent energy fractions
			jet_HF.push_back(HF);
			jet_EMF.push_back(EMF);
		}
		else if (fabs(ijet->eta()) > 2.4 && fabs(ijet->eta()) <= 3.0) {
			// push back jets outside of eta window for later inspection
			jet_pt_raw.push_back(ijet->pt()*correctionJEC);
			jet_energy_correction_raw.push_back(correctionJEC);
			jet_eta_raw.push_back(ijet->eta());
			jet_phi_raw.push_back(ijet->phi());
			jet_m_raw.push_back(ijet->m()*correctionJEC);
			jet_csv_raw.push_back(ijet->btagDiscriminator()); // keep CSV, we could re-ntuplize and implement a tagging close to RECO level recommendations
			jet_ID_raw.push_back(jetID);
	
			jet_energy_raw.push_back(jet_e*correctionJEC);
			// do not correct subsequent energy fractions
			jet_HF_raw.push_back(HF);
			jet_EMF_raw.push_back(EMF);
		}

		Ht_raw += ijet->pt()*correctionJEC;
		jet_num_raw++;
    }

	// very adhoc (I know), but need to know for 3-jet resonance analysis
	if (jet_num >= 6) {
		eventJetIdPassHisto_->Fill(2);
	}
	else if (numberGoodJets > 0 && analysis_jetID_list.size() >= 6) {
		eventJetIdPassHisto_->Fill(1);
	}
	else if (numberGoodJets > 0) {
		eventJetIdPassHisto_->Fill(0);
	}
	else {
		eventJetIdPassHisto_->Fill(-1);

		jetIdPassHisto_->Fill(-1, analysis_jetID_list.size());

//		return;
	}

	for (auto &ID: analysis_jetID_list) {
		jetIdPassHisto_->Fill(ID);
	}

//s	if (jet_num_20 < cut_nJets_min) return;

	// sort pts
	SortPts();

//    TLorentzVector offline_met(0.0, 0.0, 0.0, 0.0);
/*    for (auto &candidate: *candidates) {
        TLorentzVector vector;
        vector.SetPtEtaPhiM(candidate.pt(), candidate.eta(), candidate.phi(),
                            candidate.m());
        offline_met -= vector;
    }*/
//	MET = offline_met.Pt();
//	MET_phi = offline_met.Phi();

// bug in scouting code that did not save any MET info in 2016 runs
    MET = *handle_MET;
    MET_phi = *handle_MET_phi;
	/*
	if (electrons.isValid() && muons.isValid()) {
		for (auto &e: *electrons) {
			electron_pt.push_back(e.pt());
			electron_eta.push_back(e.eta());
			electron_phi.push_back(e.phi());

	    	}

		for (auto &m: *muons) {
			muon_pt.push_back(m.pt());
			muon_eta.push_back(m.eta()); 
			muon_phi.push_back(m.phi()); 
		}
	    electron_num = electrons->size();
	    muon_num = muons->size();
	}
	else {
		electron_num = -999;
		muon_num = -999;
	}*/

    vertex_num = vertices->size();

    rho = *handle_rho;


    //-------------- Trigger Info -----------------------------------
    triggerPassHisto_->Fill("totalEvents", 1);
    if (triggerCache_.setEvent(iEvent, iSetup)) {
        for(unsigned itrig=0; itrig<vtriggerSelector_.size(); ++itrig) {
            bool result = false;
            if (vtriggerSelector_[itrig]) {
                if (triggerCache_.configurationUpdated()) {
                    vtriggerSelector_[itrig]->init(triggerCache_);
                }
                result = (*(vtriggerSelector_[itrig]))(triggerCache_);
            }
            if (result) {
                triggerPassHisto_->Fill(vtriggerAlias_[itrig].c_str(), 1);
            }
            if (!vtriggerDuplicates_[itrig]) {
                triggerResult_->push_back(result);
            } else {
                // If trigger is a duplicate, OR result with previous trigger
                triggerResult_->back() = triggerResult_->back() || result;
            }
        }
    }

    //-------------- L1 Info -----------------------------------
    l1PassHisto_->Fill("totalEvents", 1);
    if (doL1_) {
    	l1GtUtils_->retrieveL1(iEvent,iSetup,algToken_);
		for( unsigned int iseed = 0; iseed < l1Seeds_.size(); iseed++ ) {
			bool l1htbit = 0;
			l1GtUtils_->getFinalDecisionByName(l1Seeds_[iseed], l1htbit);
			l1Result_->push_back( l1htbit );
			//Fill histogram
			if (l1htbit) {
				l1PassHisto_->Fill(l1Seeds_[iseed].c_str(), 1);
			}
     	}
	}
    
    tree->Fill();

    return;
}


// ------- method called once each job just before starting event loop  -------
void CaloScoutingNtuplizer::beginJob()
{
	cout << "CaloScoutingNtuplizer::beginJob()" << endl;
    //--- book the trigger histograms ---------
    triggerNamesHisto_ = fs_->make<TH1F>("TriggerNames", "TriggerNames", 1, 0, 1);
    triggerNamesHisto_->SetCanExtend(TH1::kAllAxes);
    for (unsigned i=0; i<vtriggerSelection_.size(); ++i) {
        triggerNamesHisto_->Fill(vtriggerSelection_[i].c_str(), 1);
    }
    triggerPassHisto_ = fs_->make<TH1F>("TriggerPass", "TriggerPass", 1, 0, 1);
    triggerPassHisto_->SetCanExtend(TH1::kAllAxes);
    triggerPassHisto_->Fill("totalEvents", 0.0);
    for (unsigned i=0; i<vtriggerAlias_.size(); ++i) {
        triggerPassHisto_->Fill(vtriggerAlias_[i].c_str(), 0.0);
    }
    l1NamesHisto_ = fs_->make<TH1F>("L1Names", "L1Names", 1, 0, 1);
    l1NamesHisto_->SetCanExtend(TH1::kAllAxes);
    for (unsigned i=0; i<l1Seeds_.size(); ++i) {
        l1NamesHisto_->Fill(l1Seeds_[i].c_str(), 1);
    }
    l1PassHisto_ = fs_->make<TH1F>("L1Pass", "L1Pass", 1, 0, 1);
    l1PassHisto_->SetCanExtend(TH1::kAllAxes);
    l1PassHisto_->Fill("totalEvents", 0.0);
    for (unsigned i=0; i<l1Seeds_.size(); ++i) {
        l1PassHisto_->Fill(l1Seeds_[i].c_str(), 0.0);
	}
	// -1 corresponds to crap events (this may or may not still be around after lumis are applied)
	eventJetIdPassHisto_ = fs_->make<TH1F>("EventJetIdPass", "EventJetIdPass", 4, -1.5, 2.5); // asking how many events will pass analysis level cuts
	jetIdPassHisto_ = fs_->make<TH1F>("JetIdPass", "jetIdPass", 4, -1.5, 2.5); // pretty much jet ID of everything that comes in to our analysis level (so we are keeping some information of jets that fail the loose cut)

 	tree = fs_->make<TTree>("events","Tree for scouting data");

    tree->Branch("HT", &Ht, "HT/F");
	tree->Branch("HT_20", &Ht_20, "HT_20/F");
	tree->Branch("HT_RAW", &Ht_raw, "HT_RAW/F");

    tree->Branch("jet_num", &jet_num, "jet_num/I");
	tree->Branch("jet_num_20", &jet_num_20, "jet_num_20/I");
	tree->Branch("jet_num_RAW", &jet_num_raw, "jet_num_raw/I");

    tree->Branch("jet_pt", &jet_pt); // in eta window
    tree->Branch("jet_eta", &jet_eta);
    tree->Branch("jet_phi", &jet_phi);
    tree->Branch("jet_m", &jet_m);
    tree->Branch("jet_csv", &jet_csv);
	tree->Branch("jet_energy_correction", &jet_energy_correction);
	tree->Branch("jet_ID", &jet_ID);

	tree->Branch("jet_energy", &jet_energy);
    tree->Branch("jet_HF", &jet_HF);
    tree->Branch("jet_EMF", &jet_EMF);


	// raw
    tree->Branch("jet_pt_raw", &jet_pt_raw); // in eta window
    tree->Branch("jet_eta_raw", &jet_eta_raw);
    tree->Branch("jet_phi_raw", &jet_phi_raw);
    tree->Branch("jet_m_raw", &jet_m_raw);
    tree->Branch("jet_csv_raw", &jet_csv_raw);
	tree->Branch("jet_energy_correction_raw", &jet_energy_correction_raw);
	tree->Branch("jet_ID_raw", &jet_ID_raw);

	tree->Branch("jet_energy_raw", &jet_energy_raw);
    tree->Branch("jet_HF_raw", &jet_HF_raw);
    tree->Branch("jet_EMF_raw", &jet_EMF_raw);

    tree->Branch("vertex_num", &vertex_num, "vertex_num/I");
    tree->Branch("rho", &rho, "rho/F");
    tree->Branch("Run", &run, "Run/I");
    tree->Branch("Lumi", &lumi, "Lumi/I");
    tree->Branch("Event", &event, "Event/I");

/*	tree->Branch("electron_num", &electron_num, "electron_num/I");
	tree->Branch("electron_pt", &electron_pt);
    tree->Branch("electron_eta", &electron_eta);
    tree->Branch("electron_phi", &electron_phi);
	tree->Branch("muon_num", &muon_num, "muon_num/I");
	tree->Branch("muon_pt", &muon_pt);
    tree->Branch("muon_eta", &muon_eta);
    tree->Branch("muon_phi", &muon_phi);*/

	tree->Branch("MET", &MET);
	tree->Branch("MET_phi", &MET_phi);

    triggerResult_ = new vector<bool>;
    l1Result_ = new vector<bool>;
    tree->Branch("triggerResult", "vector<bool>", &triggerResult_);
	tree->Branch("l1Result", "vector<bool>", &l1Result_);

}

// ------- method called once each job just after ending the event loop  -------
void CaloScoutingNtuplizer::endJob()
{
    delete triggerResult_;
	delete l1Result_;

	cout << "CaloScoutingNtuplizer::endJob()" << endl;
    for(unsigned i=0; i<vtriggerSelector_.size(); ++i) {
        delete vtriggerSelector_[i];
	}
}


// -- method fills 'descriptions' with the allowed parameters for the module  --
void CaloScoutingNtuplizer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}


void CaloScoutingNtuplizer::ResetVariables()
{


    Ht = 0.0;
	Ht_20 = 0.0;
	Ht_raw = 0.0;


    // jets w/ eta <= 2.4
    jet_num = 0;
	jet_num_20 = 0;
	jet_num_raw = 0;

    jet_pt.clear();
    jet_eta.clear();
	jet_phi.clear();
	jet_m.clear();
	jet_csv.clear();
	jet_energy_correction.clear();
	jet_ID.clear();

	jet_energy.clear();
	jet_HF.clear();
	jet_EMF.clear();


	// crap jets	
    jet_pt_raw.clear();
    jet_eta_raw.clear();
	jet_phi_raw.clear();
	jet_m_raw.clear();
	jet_csv_raw.clear();
	jet_energy_correction_raw.clear();
	jet_ID_raw.clear();

	jet_energy_raw.clear();
	jet_HF_raw.clear();
	jet_EMF_raw.clear();


	// jet w/ 2.4 < eta <= 3.0

    // electrons
    electron_num = 0;
    electron_pt.clear();
    electron_eta.clear();
    electron_phi.clear();

    // muons
    muon_num = 0;
    muon_pt.clear();
    muon_eta.clear();
    muon_phi.clear();

    triggerResult_->clear();
	l1Result_->clear();

    vertex_num = 0;

    rho = 0.0;
    MET = 0.0;
    MET_phi = 0.0;

    run = 0;
    lumi = 0;
    event = 0;

    return;
}

void CaloScoutingNtuplizer::SortPts() {
	// look into making a class that does this

	auto indices = sort_pts_algo(jet_pt, greaterThan);

    apply_permutation(jet_pt, indices);
    apply_permutation(jet_eta, indices);
	apply_permutation(jet_phi, indices);
	apply_permutation(jet_m, indices);
	apply_permutation(jet_csv, indices);
	apply_permutation(jet_energy_correction, indices);
	apply_permutation(jet_ID, indices);

	apply_permutation(jet_HF, indices);
	apply_permutation(jet_EMF, indices);

	indices = sort_pts_algo(jet_pt_raw, greaterThan);

	// crap jets	
    apply_permutation(jet_pt_raw, indices);
    apply_permutation(jet_eta_raw, indices);
	apply_permutation(jet_phi_raw, indices);
	apply_permutation(jet_m_raw, indices);
	apply_permutation(jet_csv_raw, indices);
	apply_permutation(jet_energy_correction_raw, indices);
	apply_permutation(jet_ID_raw, indices);

	apply_permutation(jet_HF_raw, indices);
	apply_permutation(jet_EMF_raw, indices);
}

int CaloScoutingNtuplizer::GetCollections(const edm::Event& iEvent)
{
    // Get collections from ntuple
    // Returns nonzero if there is a problem getting a collection

    // Get jets
    iEvent.getByToken(token_jets, jets);
    if (!jets.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find ScoutingCaloJetCollection." << endl;
	return 1;
    }

/*    iEvent.getByToken(token_candidates, candidates);
    if (!candidates.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find ScoutingParticleCollection." << endl;
	return 1;
    }*/
/*
	// Get electrons
	iEvent.getByToken(token_electrons, electrons);
	if (!electrons.isValid()) {
		throw edm::Exception(edm::errors::ProductNotFound)
		<< "Could not find ScoutingEgammaCollection." << endl;
		return 1;
	}

	// Get muons
	iEvent.getByToken(token_muons, muons);
	if (!muons.isValid()) {
		throw edm::Exception(edm::errors::ProductNotFound)
	    	<< "Could not find ScoutingMuonCollection." << endl;
		return 1;
	}*/

    // Get vertices
    iEvent.getByToken(token_vertices, vertices);
    if (!vertices.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find ScoutingVertexCollection." << endl;
	return 1;
    }

    // Get rho
    iEvent.getByToken(token_rho, handle_rho);
    if (!handle_rho.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find rho." << endl;
	return 1;
    }

    // Get MET
    iEvent.getByToken(token_MET, handle_MET);
    if (!handle_MET.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find MET." << endl;
	return 1;
    }

    iEvent.getByToken(token_MET_phi, handle_MET_phi);
    if (!handle_MET_phi.isValid()) {
        throw edm::Exception(edm::errors::ProductNotFound)
	    << "Could not find MET_phi." << endl;
	return 1;
    }

    return 0;
}

//define this as a plug-in
DEFINE_FWK_MODULE(CaloScoutingNtuplizer);
